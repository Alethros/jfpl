package alethros.jfpl;

import alethros.jfpl.datasource.DataSource;
import alethros.jfpl.datasource.FplApiDataSource;
import alethros.jfpl.datasource.LocalDataSource;
import alethros.jfpl.entities.Player;
import alethros.jfpl.entities.Team;
import org.apache.http.client.fluent.Request;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Helper class with a bunch of static utility methods
 */
@SuppressWarnings({"WeakerAccess", "unchecked"})
public final class Utils {

    private Utils(){}

    public static Map<String, Player> loadPlayers(Map<?, ?> staticData){
        return ((List<Map<String, ?>>) staticData.get("elements")).stream()
                .collect(Collectors.toMap(e -> e.get("id") instanceof String ? (String)e.get("id") : Integer.toString(((Double)e.get("id")).intValue()), Player::new));
    }

    public static void registerTeams(Map<?, ?> staticData){
        TeamsManager.reset();
        List<Map<?, ?>> teams = (List<Map<?, ?>>)staticData.get("teams");
        for (Map<?, ?> team: teams){
            String id = Integer.toString(((Double)team.get("id")).intValue());
            String name = (String)team.get("name");
            String shortName = (String)team.get("short_name");
            if (!team.containsKey("strength")){
                System.out.println("WARNING: no strength for team " + name);
                TeamsManager.registerTeam(id, new Team(id, name, shortName, 1));
                continue;
            }
            int strength = ((Double)team.get("strength")).intValue();
            TeamsManager.registerTeam(id, new Team(id, name, shortName, strength));
        }
    }

    public static DataSource loadHistoricData(File file) throws IOException {
        if (!file.isFile()){
            throw new IllegalStateException();
        }

        return new LocalDataSource(file);
    }

    public static DataSource loadPossiblyCachedDataSource(File file, Duration cacheTimeout) throws IOException {

        if (!file.isFile()){
            return new FplApiDataSource();
        }

        DataSource localData = new LocalDataSource(file);
        if (localData.lastUpdated().plus(cacheTimeout).isAfter(Instant.now())){
            return localData;
        }

        return new FplApiDataSource();

    }

    public static Double mean(Collection<Double> doubles){
        Double sum = 0.;
        for (Double d : doubles){
            sum += d;
        }
        return sum / doubles.size();
    }

    public static Double mean(double[] doubles){
        Double sum = 0.;
        for (Double d : doubles){
            sum += d;
        }
        return sum / doubles.length;
    }

    public static Double variance(Collection<Double> doubles){
        Double mean = mean(doubles);
        Double temp = 0.;
        for(Double d : doubles)
            temp += (d-mean)*(d-mean);
        return temp / (doubles.size()-1);
    }

    public static Double standardDeviation(Collection<Double> doubles){
        return Math.sqrt(variance(doubles));
    }

    public static Double calculateSSR(double[] predictions, double[] labels){
        double ssr = 0.0;
        for (int i = 0; i < predictions.length; i++) {
            double resid = labels[i] - predictions[i];
            ssr += resid * resid;
        }
        return ssr;
    }

    public static double calculateRSquared(double[] predictions, double[] labels){
        // calculate total sum of squares (derivation of y from mean)
        double yMean = mean(predictions);
        double tss = 0.0;
        for (double prediction : predictions) {
            tss += (prediction - yMean) * (prediction - yMean);
        }

        // calculate R-squared value and return
        return 1 - (calculateSSR(predictions, labels) / tss);
    }

    public static double[] toArray(List<Double> doubles){
        double[] result = new double[doubles.size()];
        for (int i = 0; i < doubles.size(); i++){
            result[i] = doubles.get(i);
        }
        return result;
    }

    public static double[][] toMatrix(List<List<Double>> doubles){
        double[][] result = new double[doubles.size()][];
        for (int i = 0; i < doubles.size(); i++){
            result[i] = toArray(doubles.get(i));
        }
        return result;
    }

    public static Double zeroIfNull(Number d){
        return d == null ? 0. : d.doubleValue();
    }

    public static String getContentsFromUrl(String url) throws IOException {
        return Request.Get(url).execute().returnContent().asString();
    }

    public static <E> List<List<E>> getKCombinations(List<E> input, int k){

        List<List<E>> subsets = new ArrayList<>();

        int[] s = new int[k];                  // here we'll keep indices pointing to elements in input array

        if (k <= input.size()) {
            // first index sequence: 0, 1, 2, ...
            for (int i = 0; (s[i] = i) < k - 1; i++);
            subsets.add(getSubset(input, s));
            for(;;) {
                int i;
                // find position of item that can be incremented
                for (i = k - 1; i >= 0 && s[i] == input.size() - k + i; i--);
                if (i < 0) {
                    break;
                }
                s[i]++;                    // increment this item
                for (++i; i < k; i++) {    // fill up remaining items
                    s[i] = s[i - 1] + 1;
                }
                subsets.add(getSubset(input, s));
            }
        }

        return subsets;
    }

    // generate actual subset by index sequence
    private static <E> List<E> getSubset(List<E> input, int[] subset) {
        List<E> result = new ArrayList<>();
        for (int i = 0; i < subset.length; i++)
            result.add(input.get(subset[i]));
        return result;
    }

    /**
     * compute pow(base, pow)
     * O(N^3) * logN
     **/
    public static double[][] squareMatrixPower(double[][] base, long pow)	{
        verifySquareMatrix(base);

        double[][] ans = new double[base.length][base.length];
        for (int i = 0; i < base.length; i++)	ans[i][i] = 1;

        while ( pow != 0 )	{
            if	( (pow&1) != 0 )	ans = multiplyMatrix(ans, base);
            base = multiplyMatrix(base, base);
            pow >>= 1;
        }

        return ans;
    }

    /**
     * compute m * m2
     * O(N^3)
     **/
    public static double[][] multiplyMatrix(double[][] m, double[][] m2)	{
        verifySquareMatrix(m);
        verifySquareMatrix(m2);
        if (m.length != m2.length) throw new IllegalArgumentException(String.format("Expected same matrix sizes [%s,%s]", m.length, m2.length));

        double[][] ans = new double[m.length][m.length];

        for (int i = 0; i < m.length; i++)	for (int j = 0; j < m.length; j++)	{
            ans[i][j] = 0;
            for (int k = 0; k < m.length; k++)	{
                ans[i][j] += m[i][k] * m2[k][j];
            }
        }

        return	ans;
    }

    /**
     * Returns the sum total of each element in the matrix
     * @param m the matrix over which to sum
     * @return the sum
     */
    public static double sumAllMatrixElements(double[][] m) {
        double sum = 0.;
        for (double[] n : m) {
            for (double o : n) {
                sum += o;
            }
        }
        return sum;
    }

    /**
     * Multiple all elements of a matrix by some scalar value.
     * @param m matrix
     * @param k scalar
     * @return resultant multipled matrix
     */
    public static double[][] multiplyMatrixElements(double[][] m, double k) {
        verifySquareMatrix(m);
        double[][] r = new double[m.length][m.length];
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m.length; j++) {
                r[i][j] = m[i][j] * k;
            }
        }
        return r;
    }

    private static void verifySquareMatrix(double[][] m) {
        int size = m.length;
        for (double[] d : m) {
            if (d.length != size) throw new IllegalArgumentException(String.format("Base matrix is not square [%s,%s]", d.length, size));
        }
    }
}
