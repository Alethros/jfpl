package alethros.jfpl.analysis;

import alethros.jfpl.*;
import alethros.jfpl.datasource.DataSource;
import alethros.jfpl.entities.Player;
import alethros.jfpl.entities.Team;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class ValueAdded {

    private static class UsefulPlayerInformation {
        private final int _nextSeasonStartPrice;
        private final int _priorSeasonTotalPoints;
        private final int _priorSeasonAppearancePoints;
        private final int _priorSeasonMinutes;
        private final Position _nextSeasonPosition;
        private UsefulPlayerInformation(int nextSeasonStartPrice, int priorSeasonTotalPoints, int priorSeasonAppearancePoints, int priorSeasonMinutes, Position nextSeasonPosition) {
            _nextSeasonStartPrice = nextSeasonStartPrice;
            _priorSeasonTotalPoints = priorSeasonTotalPoints;
            _priorSeasonAppearancePoints = priorSeasonAppearancePoints;
            _priorSeasonMinutes = priorSeasonMinutes;
            _nextSeasonPosition = nextSeasonPosition;
        }
    }

    public static void main(String[] args) throws IOException {
        Set<UsefulPlayerInformation> usefulInfo = new HashSet<>();
        Map<String, Player> priorSeasonPlayers = new HashMap<>();
        for (Season season : ImmutableSet.of(Season.S16_17, Season.S17_18, Season.S18_19)) {
            DataSource dataSource = Utils.loadHistoricData(season.storedDataPath().toFile());
            Utils.registerTeams(dataSource.staticData());
            Map<String, Player> players = Utils.loadPlayers(dataSource.staticData());
            for (GameWeek gameWeek : GameWeek.values()) {
                Map<?, ?> gameWeekData = dataSource.gameWeekData().get(gameWeek);
                if (gameWeekData == null) break;
                Map<?, ?> gameWeekElements = (Map<?, ?>) gameWeekData.get("elements");
                for (Map.Entry<String, Player> pEntry : players.entrySet()) {
                    Map<?, ?> playerGameWeekData = (Map<?, ?>) gameWeekElements.get(pEntry.getKey());
                    if (playerGameWeekData == null) continue;
                    pEntry.getValue().updateForGameWeek(gameWeek, (Map<?, ?>) playerGameWeekData.get("stats"));
                }
            }
            Set<String> playerNames = players.values().stream().map(Player::fullName).collect(ImmutableSet.toImmutableSet());
            priorSeasonPlayers.values().stream()
                    .filter(p -> playerNames.contains(p.fullName()))
                    .map(priorPlayer -> {
                        List<Player> currentPlayers = players.values().stream().filter(p -> p.fullName().equals(priorPlayer.fullName())).collect(Collectors.toList());
                        if (currentPlayers.size() != 1) return null;
                        Player currentPlayer = currentPlayers.get(0);
                        int priorSeasonTotalPoints = Arrays.stream(GameWeek.values()).map(priorPlayer::points).filter(Objects::nonNull).mapToInt(p -> p).sum();
                        int priorSeasonAppearancePoints = Arrays.stream(GameWeek.values()).map(priorPlayer::minutes).filter(Objects::nonNull).mapToInt(p -> p >= 60 ? 2 : p > 0 ? 1 : 0).sum();
                        int priorSeasonMinutes = Arrays.stream(GameWeek.values()).map(priorPlayer::minutes).filter(Objects::nonNull).mapToInt(p -> p).sum();
                        return new UsefulPlayerInformation(currentPlayer.startCost(), priorSeasonTotalPoints, priorSeasonAppearancePoints, priorSeasonMinutes, currentPlayer.position());
                    })
                    .filter(Objects::nonNull)
                    .forEach(usefulInfo::add);
            priorSeasonPlayers = players;
        }


    }

}
