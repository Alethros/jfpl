package alethros.jfpl.analysis;

import alethros.jfpl.*;
import alethros.jfpl.datasource.DataSource;
import alethros.jfpl.entities.Player;
import alethros.jfpl.entities.Team;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.SetMultimap;
import javafx.util.Pair;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class GoalkeeperRotating45s {

    private GoalkeeperRotating45s() {}

    private static void test() throws IOException {
        Map<Integer, Set<Double>> pointsVsDifficulties = new HashMap<>();
        for (Season season : ImmutableSet.of(Season.S16_17, Season.S17_18, Season.S18_19)) {

            DataSource dataSource = Utils.loadHistoricData(season.storedDataPath().toFile());
            Utils.registerTeams(dataSource.staticData());
            Map<String, Player> players = Utils.loadPlayers(dataSource.staticData());
            List<Fixture> fixtures = new ArrayList<>();

            for (GameWeek gameWeek : GameWeek.values()) {
                Map<?, ?> gameWeekData = dataSource.gameWeekData().get(gameWeek);
                if (gameWeekData == null) break;
                Map<?, ?> gameWeekElements = (Map<?, ?>) gameWeekData.get("elements");
                for (Map.Entry<String, Player> pEntry : players.entrySet()) {
                    Map<?, ?> playerGameWeekData = (Map<?, ?>) gameWeekElements.get(pEntry.getKey());
                    if (playerGameWeekData == null) continue;
                    pEntry.getValue().updateForGameWeek(gameWeek, (Map<?, ?>) playerGameWeekData.get("stats"));
                }
                List<Map<?, ?>> fixtureData = (List<Map<?, ?>>)gameWeekData.get("fixtures");
                for (Map<?, ?> fixtureMap : fixtureData){
                    Team homeTeam = TeamsManager.fromId(Integer.toString(((Double)fixtureMap.get("team_h")).intValue()));
                    Team awayTeam = TeamsManager.fromId(Integer.toString(((Double)fixtureMap.get("team_a")).intValue()));
                    Integer homeScore = fixtureMap.get("team_h_score") == null ? null : ((Double)fixtureMap.get("team_h_score")).intValue();
                    Integer awayScore = fixtureMap.get("team_a_score") == null ? null : ((Double)fixtureMap.get("team_a_score")).intValue();
                    Fixture fixture = new Fixture(gameWeek, homeTeam, awayTeam, homeScore, awayScore);
                    fixtures.add(fixture);
                }
                List<Map<?, ?>> nextFixtureData = (List<Map<?, ?>>)dataSource.staticData().get("next_event_fixtures");
                if (nextFixtureData != null) {
                    for (Map<?, ?> fixtureMap : nextFixtureData){
                        Team homeTeam = TeamsManager.fromId(Integer.toString(((Double)fixtureMap.get("team_h")).intValue()));
                        Team awayTeam = TeamsManager.fromId(Integer.toString(((Double)fixtureMap.get("team_a")).intValue()));
                        Integer homeScore = fixtureMap.get("team_h_score") == null ? null : ((Double)fixtureMap.get("team_h_score")).intValue();
                        Integer awayScore = fixtureMap.get("team_a_score") == null ? null : ((Double)fixtureMap.get("team_a_score")).intValue();
                        Fixture fixture = new Fixture(GameWeek.nextWeek(gameWeek), homeTeam, awayTeam, homeScore, awayScore);
                        fixtures.add(fixture);
                    }
                }
            }

            SetMultimap<Team, Fixture> teamFixtures = fixtures.stream()
                    .flatMap(f -> Stream.of(new Pair<>(f.homeTeam(), f), new Pair<>(f.awayTeam(), f)))
                    .collect(ImmutableSetMultimap.toImmutableSetMultimap(Pair::getKey, Pair::getValue));

            Map<Team, Double> averageOppositionScoreForTeam = Maps.transformValues(teamFixtures.entries().stream()
                    .filter(e -> teamFixtures.get(e.getKey()).stream().filter(f -> f.gameWeek().equals(e.getValue().gameWeek())).count() < 2)  // ignore double gameweeks
                    .collect(ImmutableSetMultimap.toImmutableSetMultimap(Map.Entry::getKey, e -> {
                        Team t = e.getKey();
                        Fixture f = e.getValue();
                        Team opponent = f.homeTeam().equals(t) ? f.awayTeam() : f.homeTeam();
                        return players.values().stream()
                                .filter(p -> opponent.equals(p.team(f.gameWeek())))
                                .mapToInt(p -> Optional.ofNullable(p.points(f.gameWeek())).orElse(0)).sum();
                    })).asMap(), v -> v.stream().mapToInt(i -> i).average().orElseThrow(IllegalStateException::new));

            List<Player> goalkeepers = players.values().stream()
                    .filter(p -> p.position().equals(Position.GKP))
                    .filter(p -> p.cost() < 50)
//                    .filter(p -> Arrays.stream(GameWeek.values()).mapToInt(g -> p.minutes(g) == null ? 0 : p.minutes(g)).sum() > 2000)
                    .collect(Collectors.toList());

            for (Player gk : goalkeepers) {
                for (GameWeek gw : GameWeek.values()) {
                    if (gk.minutes(gw) == null || gk.minutes(gw) < 60) continue;
                    Team t = gk.team(gw);
                    if (teamFixtures.get(t).stream().filter(f -> f.gameWeek().equals(gw)).count() > 1) continue;  // ignore double gameweeks
                    Fixture gwFixture = teamFixtures.get(t).stream().filter(f -> f.gameWeek().equals(gw)).findAny().orElseThrow(IllegalStateException::new);
                    Team opposingTeam = gwFixture.homeTeam().equals(t) ? gwFixture.awayTeam() : gwFixture.homeTeam();
                    Double opposingTeamDifficulty = averageOppositionScoreForTeam.get(opposingTeam);
                    Integer gkPoints = gk.points(gw);
                    pointsVsDifficulties.computeIfAbsent(gkPoints, k -> new HashSet<>()).add(opposingTeamDifficulty);
                }
            }
        }

//        for (Map.Entry<Integer, Set<Double>> e : pointsVsDifficulties.entrySet()) {
////            System.out.printf("%s,%s,%s%n", e.getKey(), e.getValue().size(), e.getValue().stream().mapToDouble(d -> d).average().getAsDouble());
//            System.out.printf("%s,%s%n", e.getKey(), e.getValue().stream().mapToDouble(d -> d).average().getAsDouble());
//        }
//        System.out.println();
        Map<Integer, Double> expectedPointsPerBinnedDifficulty = pointsVsDifficulties.entrySet().stream()
                .flatMap(e -> e.getValue().stream().map(e2 -> new Pair<>(e.getKey(), e2)))
                .collect(Collectors.groupingBy(e -> (int)(e.getValue()/5), Collectors.averagingInt(Pair::getKey)));
        Map<Integer, Long> numberGamesAtBinnedDifficulty = pointsVsDifficulties.entrySet().stream()
                .flatMap(e -> e.getValue().stream().map(e2 -> new Pair<>(e.getKey(), e2)))
                .collect(Collectors.groupingBy(e -> (int)(e.getValue()/5), Collectors.counting()));
        for (Map.Entry<Integer, Double> e : expectedPointsPerBinnedDifficulty.entrySet()) {
            System.out.printf("%s,%s,%s%n", e.getKey(), e.getValue(), numberGamesAtBinnedDifficulty.get(e.getKey()));
        }
    }

    public static void main(String[] args) throws IOException {
        test();
    }

}
