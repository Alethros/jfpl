package alethros.jfpl.analysis;

import alethros.jfpl.Fixture;
import alethros.jfpl.GameWeek;
import alethros.jfpl.Season;
import alethros.jfpl.Utils;
import alethros.jfpl.datasource.DataSource;
import alethros.jfpl.entities.Player;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.tuple.Pair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public final class DoesFormExist {

    private DoesFormExist() {}


    public static void formCheck() throws IOException {

        long playerSeasonPairs = 0;
        long playerSeasonWeekPairs = 0;
        long fullPlayerSeasonWeekPairs = 0;

        long aboveMedianToAbove = 0;
        long aboveMedianToBelow = 0;
        long belowMedianToAbove = 0;
        long belowMedianToBelow = 0;

        for (Season season : ImmutableSet.of(Season.S14_15, Season.S16_17, Season.S17_18)) {

            DataSource dataSource = Utils.loadHistoricData(season.storedDataPath().toFile());
            Utils.registerTeams(dataSource.staticData());
            Map<String, Player> players = Utils.loadPlayers(dataSource.staticData());

            for (GameWeek gameWeek : GameWeek.values()) {
                Map<?, ?> gameWeekData = dataSource.gameWeekData().get(gameWeek);
                if (gameWeekData == null) break;
                Map<?, ?> gameWeekElements = (Map<?, ?>) gameWeekData.get("elements");
                for (Map.Entry<String, Player> pEntry : players.entrySet()) {
                    Map<?, ?> playerGameWeekData = (Map<?, ?>) gameWeekElements.get(pEntry.getKey());
                    if (playerGameWeekData == null) continue;
                    pEntry.getValue().updateForGameWeek(gameWeek, (Map<?, ?>) playerGameWeekData.get("stats"));
                }
            }
            for (Player p : players.values()) {
                playerSeasonPairs++;
                List<Integer> points = new ArrayList<>();
                List<Pair<Integer, Integer>> pointPairs = new ArrayList<>();
                for (GameWeek gameWeek : GameWeek.values()) {
                    if (gameWeek.equals(GameWeek.GW1)) continue;  // no link
                    Integer m2 = p.minutes(gameWeek);
                    Integer m1 = p.minutes(GameWeek.previousWeek(gameWeek));
                    if (m1 == null || m2 == null) continue;
                    playerSeasonWeekPairs++;
                    if (m1 < 90 || m2 < 90) continue;
                    fullPlayerSeasonWeekPairs++;
                    points.add(p.points(gameWeek));
                    pointPairs.add(Pair.of(p.points(GameWeek.previousWeek(gameWeek)), p.points(gameWeek)));
                }
                if (points.isEmpty()) continue;
                points.sort(Comparator.comparingInt(i -> i));
                double medianPoints = points.get(points.size()/2);
                for (Pair<Integer, Integer> duo : pointPairs) {
                    if (duo.getLeft() < medianPoints) {
                        if (duo.getRight() < medianPoints) {
                            belowMedianToBelow++;
                        } else if (duo.getRight() > medianPoints){
                            belowMedianToAbove++;
                        }
                    } else if (duo.getLeft() > medianPoints){
                        if (duo.getRight() < medianPoints) {
                            aboveMedianToBelow++;
                        } else if (duo.getRight() > medianPoints){
                            aboveMedianToAbove++;
                        }
                    }
                }
            }

        }

        System.out.println("Total player-season pairs examined: " + playerSeasonPairs);
        System.out.println("Total player-season pair weeks possible: " + playerSeasonWeekPairs);
        System.out.println("Total player-season pair weeks with full 90 minutes: " + fullPlayerSeasonWeekPairs);
        System.out.println("\nAbove to above: " + aboveMedianToAbove);
        System.out.println("Below to below: " + belowMedianToBelow);
        System.out.println("Above to below: " + aboveMedianToBelow);
        System.out.println("Below to above: " + belowMedianToAbove);

    }

    public static void main(String[] args) throws IOException {
        formCheck();
    }

}
