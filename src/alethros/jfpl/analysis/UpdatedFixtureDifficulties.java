package alethros.jfpl.analysis;

import alethros.jfpl.*;
import alethros.jfpl.datasource.DataSource;
import alethros.jfpl.entities.Player;
import alethros.jfpl.entities.Team;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import javafx.geometry.Pos;

import java.io.IOException;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static alethros.jfpl.Utils.multiplyMatrixElements;
import static alethros.jfpl.Utils.squareMatrixPower;
import static alethros.jfpl.Utils.sumAllMatrixElements;

/**
 * The fixture difficulties provided by FPL are a bit crap. Here, we provide four alternative measures of fixture
 * difficulty.
 *
 * 1. table checker - this extracts a 'fair' league table taking account of how some teams have played tougher or
 *    easier fixtures so far. Only the ordering is a reliable measure here. It is of interest as a team standing measure
 *    but not so useful for further analysis rather than simply as a result. References:
 *    - https://umdrive.memphis.edu/ccrousse/public/MATH%207375/PERRON.pdf
 *    - https://www.reddit.com/r/FantasyPL/comments/584y64/geek_alert_can_anyone_help_explain_how_to_use/
 *
 * 2. offensive scored against - how attacking players have fared against this team over the prior N weeks (weighted -
 *    todo what measure here?
 *    ) using points scored from attackers.
 *
 * 3. defensive scored against - how defensive players have fared against this team over the prior N weeks using points
 *    scored from defenders.
 *
 * 4. overall scored against - how all players have fared against this team over the prior N weeks.
 */
public class UpdatedFixtureDifficulties {

    private static final Season SEASON = Season.S18_19;
    private static final Duration TIMEOUT = Duration.ofDays(10);
    private static final long INFINITY_EFFECTIVE_LIMIT = 100;

    public static void main(String[] args) throws IOException {

        // setup
        DataSource dataSource = Utils.loadPossiblyCachedDataSource(SEASON.storedDataPath().toFile(), TIMEOUT);
        Set<Fixture> fixtures = FixtureLoader.loadPlayedFixtures(dataSource);
        Set<Player> players = PlayerLoader.loadPlayers(dataSource);
        List<Team> teams = ImmutableList.copyOf(TeamsManager.availableTeams());

        // 1. PF
        Map<Team, Double> pf = getTeamRankingsWithPerronFrobenius(fixtures, teams);
        System.out.printf("%nPerron-Frobenius rankings:%n");
        for (Map.Entry<Team, Double> e : pf.entrySet()) {
            System.out.printf("%s : %s%n", e.getKey(), e.getValue());
        }

        // 2. Offensive scored against
        Map<Team, Integer> offensiveAgainst = getTeamScoredAgainstForPositions(fixtures, players, ImmutableSet.of(Position.MID, Position.FWD), GameWeek.GW1);
        System.out.printf("%nOffensive scored against rankings:%n");
        for (Map.Entry<Team, Integer> e : offensiveAgainst.entrySet()) {
            System.out.printf("%s : %s%n", e.getKey(), e.getValue());
        }

        // 3. Defensive scored against
        Map<Team, Integer> defensiveAgainst = getTeamScoredAgainstForPositions(fixtures, players, ImmutableSet.of(Position.GKP, Position.DEF), GameWeek.GW1);
        System.out.printf("%nDefensive scored against rankings:%n");
        for (Map.Entry<Team, Integer> e : defensiveAgainst.entrySet()) {
            System.out.printf("%s : %s%n", e.getKey(), e.getValue());
        }

        // 4. Total scored against
        Map<Team, Integer> totalAgainst = getTeamScoredAgainstForPositions(fixtures, players, ImmutableSet.copyOf(Arrays.asList(Position.values())), GameWeek.GW1);
        System.out.printf("%nTotal scored against rankings:%n");
        for (Map.Entry<Team, Integer> e : totalAgainst.entrySet()) {
            System.out.printf("%s : %s%n", e.getKey(), e.getValue());
        }

        // 5. Offensive scored against - last 10
        Map<Team, Integer> offensiveAgainst10 = getTeamScoredAgainstForPositions(fixtures, players, ImmutableSet.of(Position.MID, Position.FWD), GameWeek.GW14);
        System.out.printf("%nOffensive scored against last 10 rankings:%n");
        for (Map.Entry<Team, Integer> e : offensiveAgainst10.entrySet()) {
            System.out.printf("%s : %s%n", e.getKey(), e.getValue());
        }

        // 6. Defensive scored against - last 10
        Map<Team, Integer> defensiveAgainst10 = getTeamScoredAgainstForPositions(fixtures, players, ImmutableSet.of(Position.GKP, Position.DEF), GameWeek.GW14);
        System.out.printf("%nDefensive scored against last 10 rankings:%n");
        for (Map.Entry<Team, Integer> e : defensiveAgainst10.entrySet()) {
            System.out.printf("%s : %s%n", e.getKey(), e.getValue());
        }

        // 7. Total scored against - last 10
        Map<Team, Integer> totalAgainst10 = getTeamScoredAgainstForPositions(fixtures, players, ImmutableSet.copyOf(Arrays.asList(Position.values())), GameWeek.GW14);
        System.out.printf("%nTotal scored against last 10 rankings:%n");
        for (Map.Entry<Team, Integer> e : totalAgainst10.entrySet()) {
            System.out.printf("%s : %s%n", e.getKey(), e.getValue());
        }

    }

    private static Map<Team, Double> getTeamRankingsWithPerronFrobenius(Set<Fixture> fixtures, List<Team> teams) {

        Map<Team, Integer> teamIndices = IntStream.range(0, teams.size()).boxed().collect(Collectors.toMap(teams::get, k -> k));

        // populate the preference matrix
        double[][] preferenceMatrix = new double[teams.size()][teams.size()];
        for (Fixture fixture : fixtures) {
            if (fixture.homeScore() == null || fixture.awayScore() == null) continue;  // todo do this in fixture loader?
            double coefH = fixture.homeScore() > fixture.awayScore() ? 1. : fixture.homeScore().equals(fixture.awayScore()) ? 0.5 : 0.;
            coefH = getCoefficient(coefH, 1. - coefH);
            double coefA = 1. - coefH;
            int indexH = teamIndices.get(fixture.homeTeam());
            int indexA = teamIndices.get(fixture.awayTeam());
            preferenceMatrix[indexH][indexA] = coefH;
            preferenceMatrix[indexA][indexH] = coefA;
        }

        // determine the ranking eigenvector
        double[][] powerPreferences = squareMatrixPower(preferenceMatrix, INFINITY_EFFECTIVE_LIMIT);
        double[][] normalisedPreferences = multiplyMatrixElements(powerPreferences, 1. / sumAllMatrixElements(powerPreferences));
        List<Team> sortedTeams = new ArrayList<>(teams);
        sortedTeams.sort(Comparator.comparingDouble(t -> normalisedPreferences[0][teamIndices.get(t)]));
        Map<Team, Double> result = new LinkedHashMap<>();
        for (Team t : sortedTeams) {
            result.put(t, normalisedPreferences[0][teamIndices.get(t)]);
        }
        return result;
    }

    private static Map<Team, Integer> getTeamScoredAgainstForPositions(Set<Fixture> fixtures, Set<Player> players, Set<Position> positions, GameWeek minGameWeek) {
        Set<Player> filteredPlayers = players.stream()
                .filter(e -> positions.contains(e.position()))
                .collect(ImmutableSet.toImmutableSet());

        Map<Team, AtomicInteger> scored = new HashMap<>();
        for (Fixture f : fixtures) {
            if (f.gameWeek().toInt() < minGameWeek.toInt()) continue;
            scored.computeIfAbsent(f.homeTeam(), k -> new AtomicInteger()).addAndGet(filteredPlayers.stream()
                    .filter(p -> p.team(f.gameWeek()).equals(f.awayTeam()))
                    .map(p -> p.points(f.gameWeek()))
                    .filter(Objects::nonNull)
                    .mapToInt(e -> e)
                    .sum());
            scored.computeIfAbsent(f.awayTeam(), k -> new AtomicInteger()).addAndGet(filteredPlayers.stream()
                    .filter(p -> p.team(f.gameWeek()).equals(f.homeTeam()))
                    .map(p -> p.points(f.gameWeek()))
                    .filter(Objects::nonNull)
                    .mapToInt(e -> e)
                    .sum());
        }
        List<Team> sortedTeams = new ArrayList<>(scored.keySet());
        sortedTeams.sort(Comparator.comparingInt(t -> scored.get(t).get()));

        Map<Team, Integer> result = new LinkedHashMap<>();
        for (Team t : sortedTeams) {
            result.put(t, scored.get(t).get());
        }
        return result;
    }

    /**
     * Return coefficient for Perron-Frobenius preference matrix, given the relative initial coefficients.
     * @param a initial coefficient for a
     * @param b initial coefficient for b
     * @return final coefficient for a
     */
    private static double getCoefficient(double a, double b) {
        double x = (a + 1) / (a + b + 2) - 0.5;
        return 0.5 + 0.5 * (x > 0 ? 1 : x < 0 ? -1 : 0) * Math.sqrt(Math.abs(2 * x));
    }

}
