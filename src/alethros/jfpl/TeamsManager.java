package alethros.jfpl;

import alethros.jfpl.entities.Team;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class TeamsManager {

    private static Map<String, Team> _teamSet = new HashMap<>();

    public static void registerTeam(String id, Team team){
        // todo: only static per season? dont have static?
        _teamSet.put(id, team);
    }

    public static Collection<Team> availableTeams(){
        return _teamSet.values();
    }

    public static Team fromShortName(String shortName){
        return _teamSet.values().stream().filter(t -> t.shortName().equals(shortName)).findAny().orElseThrow(() -> new IllegalStateException("Unknown short name " + shortName));
    }

    public static boolean isRegistered(String shortName){
        return _teamSet.values().stream().anyMatch(t -> t.shortName().equals(shortName));
    }

    public static void reset(){
        _teamSet.clear();
    }

    public static Team fromId(String id){
        return _teamSet.computeIfAbsent(id, k -> { throw new IllegalArgumentException("Unknown team id " + id); });
    }

}
