package alethros.jfpl.datasource;

import alethros.jfpl.GameWeek;

import java.time.Instant;
import java.util.Map;

public interface DataSource {

    Instant lastUpdated();
    Map<?, ?> staticData();
    Map<GameWeek, Map<?, ?>> gameWeekData();

}
