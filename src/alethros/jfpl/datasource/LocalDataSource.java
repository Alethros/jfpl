package alethros.jfpl.datasource;

import com.google.common.collect.ImmutableMap;
import com.jsoniter.JsonIterator;
import com.jsoniter.output.JsonStream;
import org.apache.commons.io.FileUtils;
import alethros.jfpl.GameWeek;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

public class LocalDataSource implements DataSource {

    private final Instant _lastUpdated;
    private final Map<?, ?> _staticData;
    private final Map<GameWeek, Map<?, ?>> _gameWeekData;

    public LocalDataSource(File localDataFile) throws IOException {
        Map<?, ?> data = JsonIterator.deserialize(FileUtils.readFileToByteArray(localDataFile), HashMap.class);
        _lastUpdated = Instant.ofEpochMilli(((Double)data.get("lastUpdated")).longValue());
        _staticData = (Map<?, ?>)data.get("static");
        Map<String, Map<?, ?>> gameweeks = (Map<String, Map<?, ?>>) data.get("gameweeks");
        _gameWeekData = new HashMap<>();
        for (Map.Entry<String, Map<?, ?>> gameweek : gameweeks.entrySet()){
            _gameWeekData.put(GameWeek.valueOf(gameweek.getKey()), gameweek.getValue());
        }
    }

    @Override
    public Instant lastUpdated() {
        return _lastUpdated;
    }

    @Override
    public Map<?, ?> staticData() {
        return _staticData;
    }

    @Override
    public Map<GameWeek, Map<?, ?>> gameWeekData() {
        return _gameWeekData;
    }

    public static void store(DataSource datasource, File localDataFile) throws IOException {

        ImmutableMap.Builder<String, Map<?, ?>> gwBuilder = new ImmutableMap.Builder<>();
        for (Map.Entry<GameWeek, Map<?, ?>> gameweek : datasource.gameWeekData().entrySet()){
            gwBuilder.put(gameweek.getKey().toString(), gameweek.getValue());
        }

        ImmutableMap<Object, Object> mapToStore = new ImmutableMap.Builder<>()
                .put("lastUpdated", datasource.lastUpdated().toEpochMilli())
                .put("static", datasource.staticData())
                .put("gameweeks", gwBuilder.build())
                .build();
        String stringToStore = JsonStream.serialize(mapToStore);

        FileUtils.write(localDataFile, stringToStore, Charset.defaultCharset());
    }
}
