package alethros.jfpl.datasource;

import com.jsoniter.JsonIterator;
import alethros.jfpl.GameWeek;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static alethros.jfpl.Utils.getContentsFromUrl;

public class FplApiDataSource implements DataSource {

    private static final String BOOTSTRAP_STATIC_URL = "https://fantasy.premierleague.com/api/bootstrap-static";
    private static final String GAMEWEEK_URL = "https://fantasy.premierleague.com/api/event/__GAMEWEEK__/live";

    private final Instant _lastUpdated;
    private final Map<?, ?> _staticData;
    private final Map<GameWeek, Map<?, ?>> _gameWeekData;

    public FplApiDataSource() throws IOException {
        _lastUpdated = Instant.now();
        _staticData = JsonIterator.deserialize(getContentsFromUrl(BOOTSTRAP_STATIC_URL), HashMap.class);
        _gameWeekData = new HashMap<>();
        ((List<Map<?, ?>>)_staticData.get("events")).stream()
                .filter(e -> (Boolean)e.get("finished") || (Boolean)e.get("is_current"))
                .map(e -> ((Double)e.get("id")).intValue())
                .forEach(e -> {
                            try {
                                _gameWeekData.put(
                                        GameWeek.fromInt(e),
                                        JsonIterator.deserialize(
                                                getContentsFromUrl(GAMEWEEK_URL.replace("__GAMEWEEK__", Integer.toString(e))),
                                                HashMap.class
                                        ));
                            } catch (IOException e1) {
                                throw new UncheckedIOException(e1);
                            }
                        }
                );
    }

    @Override
    public Instant lastUpdated() {
        return _lastUpdated;
    }

    @Override
    public Map<?, ?> staticData() {
        return _staticData;
    }

    @Override
    public Map<GameWeek, Map<?, ?>> gameWeekData() {
        return _gameWeekData;
    }

}
