package alethros.jfpl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public enum GameWeek {
   
    GW1(1),
    GW2(2),
    GW3(3),
    GW4(4),
    GW5(5),
    GW6(6),
    GW7(7),
    GW8(8),
    GW9(9),
    GW10(10),
    GW11(11),
    GW12(12),
    GW13(13),
    GW14(14),
    GW15(15),
    GW16(16),
    GW17(17),
    GW18(18),
    GW19(19),
    GW20(20),
    GW21(21),
    GW22(22),
    GW23(23),
    GW24(24),
    GW25(25),
    GW26(26),
    GW27(27),
    GW28(28),
    GW29(29),
    GW30(30),
    GW31(31),
    GW32(32),
    GW33(33),
    GW34(34),
    GW35(35),
    GW36(36),
    GW37(37),
    GW38(38);

    private final int _id;

    GameWeek(int i){
        _id = i;
    }

    private static Map<Integer, GameWeek> _intLookup;

    static {
        _intLookup = new HashMap<>();
        Arrays.stream(GameWeek.values()).forEach(v -> _intLookup.put(v._id, v));
    }

    public static GameWeek fromInt(int v){
        if (!_intLookup.containsKey(v))
            return null;
        return _intLookup.get(v);
    }

    public int toInt(){
        return _id;
    }

    public static GameWeek previousWeek(GameWeek gw){
        if (gw.equals(GameWeek.GW1))
            return null;
        return _intLookup.get(gw._id - 1);
    }

    public static GameWeek nextWeek(GameWeek gw){
        if (gw.equals(GameWeek.GW38))
            return null;
        return _intLookup.get(gw._id + 1);
    }
}
