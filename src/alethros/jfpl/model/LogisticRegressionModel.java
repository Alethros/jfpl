package alethros.jfpl.model;

import alethros.jfpl.TrainingData;
import alethros.jfpl.feature.FeatureVectors;

import java.util.Collection;

public class LogisticRegressionModel extends Model {

    /** the learning rate */
    private double rate;

    /** the weight to learn */
    private double[] weights;

    /** the number of iterations */
    private int iterations;

    public LogisticRegressionModel() {
        this.rate = 0.0001;
        this.iterations = 3000;
    }

    private static double sigmoid(double z) {
        return 1.0 / (1.0 + Math.exp(-z));
    }

    private double classify(double[] x) {
        double logit = .0;
        for (int i=0; i<weights.length;i++)  {
            logit += weights[i] * x[i];
        }
        return sigmoid(logit);
    }

    @Override
    public Predictor _train(Collection<TrainingData> trainingData) {
        weights = new double[trainingData.stream().findAny().get().featureVector().getVector().length];
        for (int n=0; n<iterations; n++) {
            double lik = 0.0;
            for (TrainingData td : trainingData) {
                double[] x = td.featureVector().getVector();
                double predicted = classify(x);
                double label = td.label();
                for (int j=0; j<weights.length; j++) {
                    weights[j] = weights[j] + rate * (label - predicted) * x[j];
                }
                // not necessary for learning
                lik += label * Math.log(classify(x)) + (1-label) * Math.log(1- classify(x));
            }
        }

        return (p, v) -> classify(v);
    }
}
