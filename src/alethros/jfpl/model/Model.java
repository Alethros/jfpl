package alethros.jfpl.model;

import alethros.jfpl.*;
import alethros.jfpl.entities.Player;
import alethros.jfpl.feature.FeatureVectors;

import java.util.*;
import java.util.stream.Collectors;

public abstract class Model {
    private final Map<Position, Predictor> _regression;

    public abstract Predictor _train(Collection<TrainingData> trainingData);

    @FunctionalInterface
    public interface Predictor {
        double predict(Player player, double[] vector);
    }

    Model(){
        _regression = new HashMap<>();
    }

    public void train(Collection<TrainingData> trainingData){
        for (Position position : Position.values()) {

            List<TrainingData> positionalTrainingData = trainingData.stream()
                    .filter(td -> td.player().position().equals(position))
                    .collect(Collectors.toList());
            System.out.println("Training started for position " + position.longName() + " on " + positionalTrainingData.size() + " data points");

            FeatureVectorMonitor.registerTrainingSet(position, positionalTrainingData);

            double[] labels = new double[positionalTrainingData.size()];
            double[][] vectors = new double[positionalTrainingData.size()][];
            int index = 0;
            for (TrainingData td : positionalTrainingData) {
                labels[index] = td.label();
                vectors[index++] = td.featureVector().getVector();
            }

            _regression.put(position, _train(positionalTrainingData));
            System.out.println("Training complete, r_squared = " + calculateRSquared(_regression.get(position), labels, vectors));

            System.out.println("Performing cross-validation for position " + position.longName());
            crossValidate(labels, vectors, 3);
        }
    }

    public double predict(Player player, double[] featureVector){
        if (!_regression.containsKey(player.position()))
            throw new IllegalStateException("predict called before train");
        return _regression.get(player.position()).predict(player, featureVector);
    }

    private double calculateRSquared(Predictor predictor, double[] labels, double[][] vectors){
        double[] predictions = new double[vectors.length];
        for (int i = 0; i < vectors.length; i++) {
            predictions[i] = predictor.predict(null, vectors[i]);
        }
        System.out.println("Average Prediction: " + Utils.mean(predictions));
        return Utils.calculateRSquared(predictions, labels);
    }

    void crossValidate(double[] labels, double[][] vectors, int splits){

        List<double[]> splitLabels = splitIntoX(labels, splits);
        List<double[][]> splitVectors = splitIntoX(vectors, splits);

        for (int i = 0; i < splits; i++){
            double[] testLabels = splitLabels.get(i);
            double[][] testVectors = splitVectors.get(i);

            List<Double> trainLabelsList = new ArrayList<>();
            List<double[]> trainVectorsList = new ArrayList<>();
            for (int j = 0; j < splits; j++){
                if (j == i) continue;
                for (int k = 0; k < splitLabels.get(j).length; k++){
                    trainLabelsList.add(splitLabels.get(j)[k]);
                    trainVectorsList.add(splitVectors.get(j)[k]);
                }
            }
            List<TrainingData> crossTrainData = new ArrayList<>();
            for (int j = 0; j < trainLabelsList.size(); j++){
                crossTrainData.add(new TrainingData(FeatureVectors.of(trainVectorsList.get(j)), trainLabelsList.get(j)));
            }
            Predictor predictor = _train(crossTrainData);
            System.out.printf("Cross-validation %-2s: rSquared = %s\n", i, calculateRSquared(predictor, testLabels, testVectors));
        }
    }

    private List<double[]> splitIntoX(double[] input, int x){
        int size = input.length;
        int prevInd = 0;
        List<double[]> result = new ArrayList<>();
        for (int i = 1; i <= x; i++){
            int maxInd = (int) Math.floor(i * size / x);
            double[] partial = new double[maxInd - prevInd];
            System.arraycopy(input, prevInd, partial, 0, maxInd - prevInd);
            result.add(partial);
            prevInd = maxInd;
        }
        return result;
    }

    private List<double[][]> splitIntoX(double[][] input, int x){
        int size = input.length;
        int prevInd = 0;
        List<double[][]> result = new ArrayList<>();
        for (int i = 1; i <= x; i++){
            int maxInd = (int) Math.floor(i * size / x);
            double[][] partial = new double[maxInd - prevInd][];
            System.arraycopy(input, prevInd, partial, 0, maxInd - prevInd);
            result.add(partial);
            prevInd = maxInd;
        }
        return result;
    }

    double calculateMean(double[] points){
        return calculateSum(points) / points.length;
    }

    double calculateSum(double[] points){
        double sum = 0;
        for (int i = 0; i < points.length; i++){
            sum += points[i];
        }
        return sum;
    }
}
