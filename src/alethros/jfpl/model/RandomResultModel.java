package alethros.jfpl.model;

import alethros.jfpl.TrainingData;
import alethros.jfpl.feature.FeatureVector;

import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RandomResultModel extends Model {

    private static final Random RANDOM = new Random();

    @Override
    public Predictor _train(Collection<TrainingData> trainingData) {
        List<Double> labels = trainingData.stream().map(TrainingData::label).collect(Collectors.toList());
        return (p, v) -> labels.get(RANDOM.nextInt(labels.size()));
    }

    public static void main(String[] args) {
        RandomResultModel r = new RandomResultModel();
        List<TrainingData> d = Stream.generate(RandomResultModel::getRandomTrainingData).limit(1000000).collect(Collectors.toList());
        double[][] fvs = d.stream().map(t -> t.featureVector().getVector()).toArray(double[][]::new);
        double[] lbs = new double[d.size()];
        for (int i = 0; i < d.size(); i++) {
            lbs[i] = d.get(i).label();
        }
        r.crossValidate(lbs, fvs, 3);
    }

    private static TrainingData getRandomTrainingData(){
        double[] d = new double[20];
        double l = -1;
        while (l < 0) {
            l = 5 + RANDOM.nextGaussian() * 3;
        }
        return new TrainingData(() -> d, l);
    }
}
