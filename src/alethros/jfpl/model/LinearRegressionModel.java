package alethros.jfpl.model;

import alethros.jfpl.TrainingData;
import alethros.jfpl.Utils;
import org.apache.commons.lang3.tuple.Pair;
import smile.regression.RidgeRegression;

import java.util.*;

public class LinearRegressionModel extends Model {

    @Override
    public Predictor _train(Collection<TrainingData> trainingData){

        double[] trainingLabels = new double[trainingData.size()];
        double[][] trainingVectors = new double[trainingData.size()][];
        int index = 0;
        for (TrainingData td : trainingData) {
            trainingLabels[index] = td.label();
            trainingVectors[index++] = td.featureVector().getVector();
        }

        // remove zero columns
        Pair<double[][], List<Integer>> cleanPair = cleanZeroColumns(trainingVectors);
        double[][] cleanedTrainingVectors = cleanPair.getLeft();
        List<Integer> colsToKeep = cleanPair.getRight();

        // determine normalization factors in totals array
        double[] normalizationFactors = new double[cleanedTrainingVectors[0].length];
        Arrays.fill(normalizationFactors, 0);
        for (double[] cleanedTrainingVector : cleanedTrainingVectors) {
            for (int j = 0; j < cleanedTrainingVector.length; j++) {
                normalizationFactors[j] += cleanedTrainingVector[j];
            }
        }
        for (int j = 0; j < normalizationFactors.length; j++){
            normalizationFactors[j] = ((double)cleanedTrainingVectors.length) / normalizationFactors[j];
        }

        // normalize the training data
        for (int i = 0; i < cleanedTrainingVectors.length; i++){
            for (int j = 0; j < cleanedTrainingVectors[i].length; j++){
                cleanedTrainingVectors[i][j] *= normalizationFactors[j];
            }
        }

        RidgeRegression regression = new RidgeRegression(cleanedTrainingVectors, trainingLabels, 1);

        double[] coefficients = regression.coefficients();

        System.out.println("Average label: " + Utils.mean(trainingLabels));

        return(p, v) -> {

            double[] cleanedVector = _cleanZeroColumns(v, colsToKeep);
            return regression.predict(cleanedVector);
//            double[] contributions = new double[coefficients.length];
//            double prediction = coefficients[0];
//            contributions[0] = coefficients[0];
//            for (int i = 1; i < coefficients.length; i++){
//                prediction += coefficients[i] * cleanedVector[i-1] * normalizationFactors[i-1];
//                contributions[i] = coefficients[i] * cleanedVector[i-1] * normalizationFactors[i-1];
//            }
//            return prediction;
        };
    }

    private Pair<double[][], List<Integer>> cleanZeroColumns(double[][] vectors){
        List<Integer> colsToKeep = new ArrayList<>();
        for (int i = 0; i < vectors[0].length; i++){
            for (double[] vector : vectors) {
                if (vector[i] != 0 && vector[i] != Double.NaN) {
                    colsToKeep.add(i);
                    break;
                }
            }
        }
        double[][] result = new double[vectors.length][];
        for (int j = 0; j < vectors.length; j ++) {
            result[j] = _cleanZeroColumns(vectors[j], colsToKeep);
        }
        return Pair.of(result, colsToKeep);
    }

    private double[] _cleanZeroColumns(double[] input, List<Integer> colsToKeep){
        double[] reducedVector = new double[colsToKeep.size()];
        for (int k = 0; k < colsToKeep.size(); k++) {
            reducedVector[k] = input[colsToKeep.get(k)];
        }
        return reducedVector;
    }
}
