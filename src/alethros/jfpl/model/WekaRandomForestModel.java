package alethros.jfpl.model;

import alethros.jfpl.TrainingData;
import weka.classifiers.trees.RandomForest;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicInteger;

public class WekaRandomForestModel extends Model {

    private static final AtomicInteger _instantiationCount = new AtomicInteger(0);

    @Override
    public Predictor _train(Collection<TrainingData> trainingData){

        double[] trainingLabels = new double[trainingData.size()];
        double[][] trainingVectors = new double[trainingData.size()][];
        int index = 0;
        for (TrainingData td : trainingData) {
            trainingLabels[index] = td.label();
            trainingVectors[index++] = td.featureVector().getVector();
        }

        RandomForest forest = new RandomForest();
        forest.setNumIterations(100);
        forest.setMaxDepth(10);
        forest.setNumFeatures(10);
        int size = trainingVectors[0].length;  // todo: address index bounds exception possibility

        ArrayList<Attribute> attributes = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            attributes.add(new Attribute("_" + Integer.toString(i)));
        }
        attributes.add(new Attribute("_label"));

        Instances data = new Instances(Integer.toString(_instantiationCount.getAndIncrement()), attributes, trainingLabels.length);
        data.setClassIndex(attributes.size() - 1);

        for (int i = 0; i < trainingLabels.length; i++){
            data.add(getInstance(trainingVectors[i], trainingLabels[i]));
        }

        try {
            forest.buildClassifier(data);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }

        return (p, vector) -> {
            Instance toPredict =  getInstance(vector, null);
            toPredict.setDataset(data);
            try {
                return forest.distributionForInstance(toPredict)[0];
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };
    }

    private Instance getInstance(double[] fv, Double label){
        Instance instance = new DenseInstance(fv.length + 1);
        for (int i = 0; i < fv.length; i++){
            instance.setValue(i, fv[i]);
        }
        if (label != null) {
            instance.setValue(fv.length, label);
        }
        return instance;
    }

}
