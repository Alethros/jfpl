package alethros.jfpl.model;

import alethros.jfpl.TrainingData;
import edu.berkeley.compbio.jlibsvm.*;
import edu.berkeley.compbio.jlibsvm.kernel.GaussianRBFKernel;
import edu.berkeley.compbio.jlibsvm.kernel.LinearKernel;
import edu.berkeley.compbio.jlibsvm.kernel.SigmoidKernel;
import edu.berkeley.compbio.jlibsvm.regression.*;
import edu.berkeley.compbio.jlibsvm.util.SparseVector;

import java.util.Collection;
import java.util.Collections;

public class SupportVectorMachineModel extends Model {

    @Override
    public Predictor _train(Collection<TrainingData> trainingData){

        double[] trainingLabels = new double[trainingData.size()];
        double[][] trainingVectors = new double[trainingData.size()][];
        int index = 0;
        for (TrainingData td : trainingData) {
            trainingLabels[index] = td.label();
            trainingVectors[index++] = td.featureVector().getVector();
        }
        MutableRegressionProblemImpl<SparseVector> problem = new MutableRegressionProblemImpl<>(trainingLabels.length);
        for (int i = 0; i < trainingLabels.length; i++) {
            problem.addExample(generateFeatures(trainingVectors[i]), (float) trainingLabels[i]);
        }
        ImmutableSvmParameterGrid.Builder builder = ImmutableSvmParameterGrid.builder();
        builder.kernelSet = Collections.singletonList(new GaussianRBFKernel(0.5f));
        builder.eps = 0.001f; // epsilon
        ImmutableSvmParameter params = builder.build();

        RegressionSVM svm = new EpsilonSVR();
        RegressionModel<SparseVector> model = svm.train(problem, params);
        return (p, v) -> model.predictValue(generateFeatures(v));
    }

    /**
     * Helper function to generate a single featureset.
     */
    private SparseVector generateFeatures(double[] doubles) {
        SparseVector sparseVector = new SparseVector(doubles.length);
        int[] indices = new int[doubles.length];
        float[] floats = new float[doubles.length];
        for (int i = 0; i < doubles.length; i++) {
            indices[i] = i;
            floats[i] = (float) doubles[i];
        }
        sparseVector.indexes = indices;
        sparseVector.values = floats;
        return sparseVector;
    }
}
