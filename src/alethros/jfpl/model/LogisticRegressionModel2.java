package alethros.jfpl.model;

import alethros.jfpl.TrainingData;
import alethros.jfpl.feature.FeatureVectors;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.tuple.Pair;
import smile.classification.LogisticRegression;

import java.util.*;

public class LogisticRegressionModel2 extends Model {



    @Override
    public Predictor _train(Collection<TrainingData> trainingData) {
        int[] trainingLabels = new int[trainingData.size()];
        double[][] trainingVectors = new double[trainingData.size()][];
        int index = 0;
        for (TrainingData td : trainingData) {
            trainingLabels[index] = (int)td.label();
            trainingVectors[index++] = td.featureVector().getVector();
        }

        // remove zero columns
        Pair<double[][], List<Integer>> cleanPair = cleanZeroColumns(trainingVectors);
        double[][] cleanedTrainingVectors = cleanPair.getLeft();
        List<Integer> colsToKeep = cleanPair.getRight();

        // determine normalization factors in totals array
        double[] normalizationFactors = new double[cleanedTrainingVectors[0].length];
        Arrays.fill(normalizationFactors, 0);
        for (double[] cleanedTrainingVector : cleanedTrainingVectors) {
            for (int j = 0; j < cleanedTrainingVector.length; j++) {
                normalizationFactors[j] += cleanedTrainingVector[j];
            }
        }
        for (int j = 0; j < normalizationFactors.length; j++){
            normalizationFactors[j] = ((double)cleanedTrainingVectors.length) / normalizationFactors[j];
        }

        // normalize the training data
        for (int i = 0; i < cleanedTrainingVectors.length; i++){
            for (int j = 0; j < cleanedTrainingVectors[i].length; j++){
                cleanedTrainingVectors[i][j] *= normalizationFactors[j];
            }
        }

        // ensure all labels are positive
        int labelOffset = -Math.min(0, (int)Collections.min(Arrays.asList(ArrayUtils.toObject(trainingLabels))));
        if (labelOffset != 0) {
            for (int i = 0; i < trainingLabels.length; i++) {
                trainingLabels[i] += labelOffset;
            }
        }

        LogisticRegression r = new LogisticRegression(cleanedTrainingVectors, trainingLabels);

        return(p, v) -> {

            double[] cleanedVector = _cleanZeroColumns(v, colsToKeep);
            return r.predict(cleanedVector) - labelOffset;
        };
    }

    private Pair<double[][], List<Integer>> cleanZeroColumns(double[][] vectors){
        List<Integer> colsToKeep = new ArrayList<>();
        for (int i = 0; i < vectors[0].length; i++){
            for (double[] vector : vectors) {
                if (vector[i] != 0 && vector[i] != Double.NaN) {
                    colsToKeep.add(i);
                    break;
                }
            }
        }
        double[][] result = new double[vectors.length][];
        for (int j = 0; j < vectors.length; j ++) {
            result[j] = _cleanZeroColumns(vectors[j], colsToKeep);
        }
        return Pair.of(result, colsToKeep);
    }

    private double[] _cleanZeroColumns(double[] input, List<Integer> colsToKeep){
        double[] reducedVector = new double[colsToKeep.size()];
        for (int k = 0; k < colsToKeep.size(); k++) {
            reducedVector[k] = input[colsToKeep.get(k)];
        }
        return reducedVector;
    }
}
