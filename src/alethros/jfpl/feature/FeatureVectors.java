package alethros.jfpl.feature;

import alethros.jfpl.GameWeek;
import alethros.jfpl.entities.Player;
import com.google.common.collect.Streams;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public abstract class FeatureVectors {

    private FeatureVectors() {}

    public static FeatureVector extractVector(GameWeek gw, Player player) {
        switch (player.position()) {
            case GKP:
                return new GoalkeeperFeatureVector(gw, player);
            case DEF:
                return new DefenderFeatureVector(gw, player);
            case MID:
                return new MidfielderFeatureVector(gw, player);
            case FWD:
                return new ForwardFeatureVector(gw, player);
            default:
                throw new IllegalArgumentException("Don't know how to treat player position: " + player.position());
        }
    }

    public static FeatureVector of(double[] vector) {
        return new InitializedFeatureVector(vector);
    }

    private static abstract class CoreFeatureVector implements FeatureVector {

        private final double[] _coreFeatures;

        private CoreFeatureVector(GameWeek gw, Player player) {

             /* capture of "nailed-on ness"
             * - confidence level needed?
             * - from last 10 weeks:
             *       played 80%+ minutes?
             *       temp injury then straight back in team?
             */
            Set<GameWeek> previous9Weeks = IntStream.range(gw.toInt() - 9, gw.toInt()).mapToObj(GameWeek::fromInt).filter(Objects::nonNull).collect(Collectors.toSet());
            Map<GameWeek, Integer> nGames = previous9Weeks.stream().collect(Collectors.toMap(w -> w, w -> player.team(w) == null ? 0 : player.team(w).numberGames(w)));
            long absencesLast9 = 0, absencesLast3 = 0;
            long subsOnLast9 = 0, subsOnLast3 = 0;
            long subsOffLast9 = 0, subsOffLast3 = 0;
            long fullMinsLast9 = 0, fullMinsLast3 = 0;
            long bps9 = 0, bps3 = 0;
            long i9 = 0, c9 = 0, t9 = 0;
            for (GameWeek w : previous9Weeks) {
                if (player.minutes(w) == null || nGames.get(w) == 0) {
                    absencesLast9++;
                    continue;
                }
                boolean last3 = gw.toInt() - w.toInt() <= 3;
                double averageMins = player.minutes(w).doubleValue() / nGames.get(w);
                if (averageMins == 0.) {
                    absencesLast9++;
                    if (last3)
                        absencesLast3++;
                } else if (averageMins < 45) {
                    subsOnLast9++;
                    if (last3)
                        subsOnLast3++;
                } else if (averageMins < 90) {
                    subsOffLast9++;
                    if (last3)
                        subsOffLast3++;
                } else if (averageMins == 90.) {
                    fullMinsLast9++;
                    if (last3)
                        fullMinsLast3++;
                } else {
                    throw new IllegalStateException("Too high average "  + averageMins + " for player " + player + " in " + gw);
                }

                long bpsWeek = player.bps(w).longValue();
                bps9 += bpsWeek;
                if (last3) {
                    bps3 += bpsWeek;
                }
                i9 += player.influence(w);
                c9 += player.creativity(w);
                t9 += player.threat(w);

                // normalization
                long wksPlayed9 = 9 - absencesLast9;
                long wksPlayed3 = 3 - absencesLast3;
                if (wksPlayed9 > 0) {
                    bps9 /= wksPlayed9;
                    i9 /= wksPlayed9;
                    c9 /= wksPlayed9;
                    t9 /= wksPlayed9;
                }
                if (wksPlayed3 > 0) {
                    bps3 /= wksPlayed3;
                }

            }

            long minutesWeight9 = subsOnLast9 + 2 * subsOffLast9 + 3 * fullMinsLast9;
            long minutesWeight3 = subsOnLast3 + 2 * subsOffLast3 + 3 * fullMinsLast3;
            double minutesAcceleration =
                    minutesWeight9 == 0 ? 0 :
                            (double)minutesWeight3 / minutesWeight9;

            /* fixture difficulty next week
             */
            Set<Double> nextWeekFixtureDifficulties = player.team(gw).futureFixtureDifficulties(gw, GameWeek.nextWeek(gw));  // should never be empty, we should only call this method if label available!
            int numberFixturesNextWeek = nextWeekFixtureDifficulties.size();
            if (numberFixturesNextWeek == 0)
                throw new IllegalStateException("No fixtures in week " + GameWeek.nextWeek(gw) + " for team " + player.team(gw));
            double averageFixtureDifficulty = nextWeekFixtureDifficulties.stream().mapToDouble(d -> d).average().orElseThrow(IllegalStateException::new);

            /*
             *
             */
            _coreFeatures = new double[] {
                    // nailed on?
                    absencesLast9,
                    subsOnLast9,
                    subsOffLast9,
                    fullMinsLast9,
                    minutesAcceleration,
                    // next weeks fixture
                    numberFixturesNextWeek,
                    averageFixtureDifficulty,
                    // bps
                    bps9,
                    // ict
                    i9,
                    c9,
                    t9
            };
        }

        @Override
        public double[] getVector() {
            return _coreFeatures;
        }

    }

    private static class DefenderFeatureVector extends CoreFeatureVector {

        private final double[] _additionalFeatures;

        private DefenderFeatureVector(GameWeek gw, Player player) {
            super(gw, player);

            Set<GameWeek> previous9Weeks = IntStream.range(gw.toInt() - 9, gw.toInt()).mapToObj(GameWeek::fromInt).filter(Objects::nonNull).collect(Collectors.toSet());
            Map<GameWeek, Integer> nGames = previous9Weeks.stream().collect(Collectors.toMap(w -> w, w -> player.team(w) == null ? 0 : player.team(w).numberGames(w)));
            int wksPlayed9 = 0, wksPlayed3 = 0;
            long cleanSheets9 = 0, cleanSheets3 = 0;
            long goalsConceded9 = 0, goalsConceded3 = 0;
            for (GameWeek w : previous9Weeks) {
                if (player.minutes(w) == null || nGames.get(w) == 0) {
                    continue;
                }
                wksPlayed9++;
                boolean last3 = gw.toInt() - w.toInt() <= 3;
                long csWeek = player.cleanSheets(w).longValue();
                cleanSheets9 += csWeek;
                long gcWeek = player.goalsConceded(w).longValue();
                goalsConceded9 += gcWeek;
                if (last3) {
                    wksPlayed3++;
                    cleanSheets3 += csWeek;
                    goalsConceded3 += gcWeek;
                }
            }
            if (wksPlayed9 > 0) {
                cleanSheets9 /= wksPlayed9;
                goalsConceded9 /= wksPlayed9;
            }
            if (wksPlayed3 > 0) {
                cleanSheets3 /= wksPlayed3;
                goalsConceded3 /= wksPlayed3;
            }
            double cleanSheetsAcceleration =
                    cleanSheets9 == 0 ? 0 :
                            (double)cleanSheets3 / cleanSheets9;
            double goalsConcededAcceleration =
                    goalsConceded9 == 0 ? 0 :
                            (double) goalsConceded3 / goalsConceded9;

            _additionalFeatures = new double[]{
                    cleanSheets9,
                    cleanSheetsAcceleration,
                    goalsConceded9,
                    goalsConcededAcceleration
            };

        }

        @Override
        public double[] getVector() {
            return Streams.concat(Arrays.stream(super.getVector()), Arrays.stream(_additionalFeatures)).toArray();
        }

    }

    private static class GoalkeeperFeatureVector extends DefenderFeatureVector {  // n.b. built on top of DefVector, changes will propagate!

        private final double[] _additionalFeatures;

        private GoalkeeperFeatureVector(GameWeek gw, Player player) {
            super(gw, player);

            Set<GameWeek> previous9Weeks = IntStream.range(gw.toInt() - 9, gw.toInt()).mapToObj(GameWeek::fromInt).filter(Objects::nonNull).collect(Collectors.toSet());
            Map<GameWeek, Integer> nGames = previous9Weeks.stream().collect(Collectors.toMap(w -> w, w -> player.team(w) == null ? 0 : player.team(w).numberGames(w)));
            int wksPlayed9 = 0, wksPlayed3 = 0;
            long saves9 = 0, saves3 = 0;
            for (GameWeek w : previous9Weeks) {
                if (player.minutes(w) == null || nGames.get(w) == 0) {
                    continue;
                }
                wksPlayed9++;
                boolean last3 = gw.toInt() - w.toInt() <= 3;
                long savesWeek = player.saves(w).longValue();
                saves9 += savesWeek;
                if (last3) {
                    wksPlayed3++;
                    saves3 += savesWeek;
                }
            }
            if (wksPlayed9 > 0) {
                saves9 /= wksPlayed9;
            }
            if (wksPlayed3 > 0) {
                saves3 /= wksPlayed3;
            }
            double saveAcceleration =
                    saves9 == 0 ? 0 :
                            (double)saves3 / saves9;

            _additionalFeatures = new double[]{
                    saves9,
                    saveAcceleration
            };
        }

        @Override
        public double[] getVector() {
            return Streams.concat(Arrays.stream(super.getVector()), Arrays.stream(_additionalFeatures)).toArray();
        }

    }

    private static class ForwardFeatureVector extends CoreFeatureVector {

        private final double[] _additionalFeatures;

        private ForwardFeatureVector(GameWeek gw, Player player) {
            super(gw, player);

            Set<GameWeek> previous9Weeks = IntStream.range(gw.toInt() - 9, gw.toInt()).mapToObj(GameWeek::fromInt).filter(Objects::nonNull).collect(Collectors.toSet());
            Map<GameWeek, Integer> nGames = previous9Weeks.stream().collect(Collectors.toMap(w -> w, w -> player.team(w) == null ? 0 : player.team(w).numberGames(w)));
            int wksPlayed9 = 0, wksPlayed3 = 0;
            long goals9 = 0, goals3 = 0;
            long assists9 = 0, assists3 = 0;
            for (GameWeek w : previous9Weeks) {
                if (player.minutes(w) == null || nGames.get(w) == 0) {
                    continue;
                }
                wksPlayed9++;
                boolean last3 = gw.toInt() - w.toInt() <= 3;
                long goalsWeek = player.goalsScored(w).longValue();
                goals9 += goalsWeek;
                long assistsWeek = player.assists(w).longValue();
                assists9 += assistsWeek;
                if (last3) {
                    wksPlayed3++;
                    goals3 += goalsWeek;
                    assists3 += assistsWeek;
                }
            }
            if (wksPlayed9 > 0) {
                goals9 /= wksPlayed9;
                assists9 /= wksPlayed9;
            }
            if (wksPlayed3 > 0) {
                goals3 /= wksPlayed3;
                assists3 /= wksPlayed3;
            }
            double goalAcceleration =
                    goals9 == 0 ? 0 :
                            (double)goals3 / goals9;
            double assistsAcceleration =
                    assists9 == 0 ? 0 :
                            (double) assists3 / assists9;

            _additionalFeatures = new double[]{
                    goals9,
                    goalAcceleration,
                    assists9,
                    assistsAcceleration
            };
        }

        @Override
        public double[] getVector() {
            return Streams.concat(Arrays.stream(super.getVector()), Arrays.stream(_additionalFeatures)).toArray();
        }

    }

    private static class MidfielderFeatureVector extends ForwardFeatureVector {  // no additions yet

        private MidfielderFeatureVector(GameWeek gw, Player player) {
            super(gw, player);
        }
    }

    private static class InitializedFeatureVector implements FeatureVector {

        private final double[] _vector;

        private InitializedFeatureVector(double[] vector) {
            _vector = vector;
        }

        @Override
        public double[] getVector() {
            return new double[0];
        }
    }
}
