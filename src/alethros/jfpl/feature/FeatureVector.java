package alethros.jfpl.feature;

public interface FeatureVector {
    double[] getVector();
}