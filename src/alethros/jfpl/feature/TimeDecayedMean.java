package alethros.jfpl.feature;

public class TimeDecayedMean {

    private final double _tau;
    private double _sum;
    private double _count;

    public TimeDecayedMean(double decayPeriod){
        _tau = 1/decayPeriod;
        _sum = 0.;
        _count = 0.;
    }

    public void update(double val, double dt){
        _sum = Math.exp( -dt / _tau) * _sum + val;
        _count = Math.exp( -dt / _tau) * _count + 1;
    }

    public double mean(){
        return _sum / _count;
    }

}
