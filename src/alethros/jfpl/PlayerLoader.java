package alethros.jfpl;

import alethros.jfpl.datasource.DataSource;
import alethros.jfpl.entities.Player;
import com.google.common.collect.ImmutableSet;

import java.util.Map;
import java.util.Set;

public final class PlayerLoader {

    private PlayerLoader() {}

    public static Set<Player> loadPlayers(DataSource dataSource) {
        Utils.registerTeams(dataSource.staticData());
        Map<String, Player> players = Utils.loadPlayers(dataSource.staticData());

        for (GameWeek gameWeek : GameWeek.values()) {

            Map<?, ?> gameWeekData = dataSource.gameWeekData().get(gameWeek);

            if (gameWeekData == null) {
                System.out.println("No data for gameweek " + gameWeek + ", breaking loop");
                break;
            }

            Map<?, ?> gameWeekElements = (Map<?, ?>) gameWeekData.get("elements");
            for (Map.Entry<String, Player> pEntry : players.entrySet()) {
                Map<?, ?> playerGameWeekData = (Map<?, ?>) gameWeekElements.get(pEntry.getKey());
                if (playerGameWeekData == null) {
                    continue;  // player wasn't in game this gameweek, or gameweek doesn't exist yet
                }
                pEntry.getValue().updateForGameWeek(gameWeek, (Map<?, ?>) playerGameWeekData.get("stats"));
            }
        }

        return ImmutableSet.copyOf(players.values());
    }

}
