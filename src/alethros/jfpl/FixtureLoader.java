package alethros.jfpl;

import alethros.jfpl.datasource.DataSource;
import alethros.jfpl.entities.Player;
import alethros.jfpl.entities.Team;
import com.google.common.collect.ImmutableSet;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class FixtureLoader {

    private FixtureLoader() {}

    public static Set<Fixture> loadPlayedFixtures(DataSource dataSource) {
        Utils.registerTeams(dataSource.staticData());
        Map<String, Player> players = Utils.loadPlayers(dataSource.staticData());
        Set<Fixture> fixtures = new HashSet<>();

        for (GameWeek gameWeek : GameWeek.values()) {

            Map<?, ?> gameWeekData = dataSource.gameWeekData().get(gameWeek);

            if (gameWeekData == null) {
                System.out.println("No data for gameweek " + gameWeek + ", breaking loop");
                break;
            }

            Map<?, ?> gameWeekElements = (Map<?, ?>) gameWeekData.get("elements");
            for (Map.Entry<String, Player> pEntry : players.entrySet()) {
                Map<?, ?> playerGameWeekData = (Map<?, ?>) gameWeekElements.get(pEntry.getKey());
                if (playerGameWeekData == null) {
                    continue;  // player wasn't in game this gameweek, or gameweek doesn't exist yet
                }
                pEntry.getValue().updateForGameWeek(gameWeek, (Map<?, ?>) playerGameWeekData.get("stats"));
            }
            List<Map<?, ?>> fixtureData = (List<Map<?, ?>>)gameWeekData.get("fixtures");
            for (Map<?, ?> fixtureMap : fixtureData){
                Team homeTeam = TeamsManager.fromId(Integer.toString(((Double)fixtureMap.get("team_h")).intValue()));
                Team awayTeam = TeamsManager.fromId(Integer.toString(((Double)fixtureMap.get("team_a")).intValue()));
                Integer homeScore = fixtureMap.get("team_h_score") == null ? null : ((Double)fixtureMap.get("team_h_score")).intValue();
                Integer awayScore = fixtureMap.get("team_a_score") == null ? null : ((Double)fixtureMap.get("team_a_score")).intValue();
                Fixture fixture = new Fixture(gameWeek, homeTeam, awayTeam, homeScore, awayScore);
                fixtures.add(fixture);
            }
        }
        return ImmutableSet.copyOf(fixtures);
    }

}
