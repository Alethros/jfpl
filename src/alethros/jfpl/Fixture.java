package alethros.jfpl;

import alethros.jfpl.entities.Team;

public class Fixture {

    private final GameWeek _gw;
    private final Team _home;
    private final Team _away;
    private final Integer _homeScore;
    private final Integer _awayScore;
    private final Boolean _played;

    public Fixture(GameWeek gw, Team home, Team away, Integer homeScore, Integer awayScore){
        _gw = gw;
        _home = home;
        _away = away;
        _homeScore = homeScore;
        _awayScore = awayScore;
        _played = homeScore != null && awayScore != null;
    }

    public GameWeek gameWeek(){
        return _gw;
    }

    public Team homeTeam(){
        return _home;
    }

    public Team awayTeam(){
        return _away;
    }

    public Integer homeScore(){
        return _homeScore;
    }

    public Integer awayScore(){
        return _awayScore;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Fixture fixture = (Fixture) o;

        if (_gw != fixture._gw) return false;
        if (!_home.equals(fixture._home)) return false;
        if (!_away.equals(fixture._away)) return false;
        if (_homeScore != null ? !_homeScore.equals(fixture._homeScore) : fixture._homeScore != null) return false;
        if (_awayScore != null ? !_awayScore.equals(fixture._awayScore) : fixture._awayScore != null) return false;
        return _played.equals(fixture._played);
    }

    @Override
    public int hashCode() {
        int result = _gw.hashCode();
        result = 31 * result + _home.hashCode();
        result = 31 * result + _away.hashCode();
        result = 31 * result + (_homeScore != null ? _homeScore.hashCode() : 0);
        result = 31 * result + (_awayScore != null ? _awayScore.hashCode() : 0);
        result = 31 * result + _played.hashCode();
        return result;
    }
}
