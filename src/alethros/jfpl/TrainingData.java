package alethros.jfpl;

import alethros.jfpl.entities.Player;
import alethros.jfpl.feature.FeatureVector;

public class TrainingData {

    private final FeatureVector _featureVector;
    private final double _label;
    private final GameWeek _gameWeek;
    private final Player _player;

    public TrainingData(FeatureVector vector, double label, GameWeek gameWeek, Player player){
        _featureVector = vector;
        _label = label;
        _gameWeek = gameWeek;
        _player = player;
    }

    public TrainingData(FeatureVector featureVector, double label){
        _featureVector = featureVector;
        _label = label;
        _gameWeek = null;
        _player = null;
    }

    public FeatureVector featureVector() {
        return _featureVector;
    }

    public double label() {
        return _label;
    }

    public Player player() {
        return _player;
    }
}
