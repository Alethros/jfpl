package alethros.jfpl;

import java.util.HashMap;
import java.util.Map;

/**
 * Supported positions and associated information
 */
public enum Position {

    GKP(1, "GKP", "Goalkeeper", 2),
    DEF(2, "DEF", "Defender", 5),
    MID(3, "MID", "Midfielder", 5),
    FWD(4, "FWD", "Forward", 3);

    private int _id;
    private String _shortName;
    private String _name;
    private int _expectedNumber;

    private static final Map<Integer, Position> _idMap = new HashMap<>();
    private static final Map<String, Position> _snMap = new HashMap<>();

    static {
        for (Position p : Position.values()) {
            _idMap.put(p._id, p);
            _snMap.put(p._shortName, p);
        }
    }

    Position(int id, String shortName, String name, int expectedNumber){
        _id = id;
        _shortName = shortName;
        _name = name;
        _expectedNumber = expectedNumber;
    }

    public int id() {
        return _id;
    }

    public String shortName() {
        return _shortName;
    }

    public String longName() {
        return _name;
    }

    public int expectedNumber() {
        return _expectedNumber;
    }

    public static Position fromId(int id){
        return _idMap.computeIfAbsent(id, k -> { throw new IllegalArgumentException("Unknown position id " + id); });
    }

    public static Position fromShortName(String shortName){
        return _snMap.computeIfAbsent(shortName, k -> {throw new IllegalStateException("Unknown short name " + shortName);});
    }

}
