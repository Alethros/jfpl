package alethros.jfpl;

import alethros.jfpl.datasource.DataSource;
import alethros.jfpl.datasource.LocalDataSource;
import alethros.jfpl.entities.Player;
import alethros.jfpl.entities.Team;
import alethros.jfpl.feature.FeatureVector;
import alethros.jfpl.feature.FeatureVectors;
import alethros.jfpl.model.*;
import com.google.common.collect.ImmutableSet;
import lpsolve.LpSolveException;

import java.io.IOException;
import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

public class Runner {

    private final Class<? extends Model> _modelClass;
    private final Season _currentSeason;
    private final Set<Season> _trainingSeasons;
    private final int _requiredHistory;
    private final int _futureConsidered;
    private final Duration _cacheTimeout;

    private Runner(Builder builder){
        _modelClass = builder._modelClass;
        _currentSeason = builder._currentSeason;
        _trainingSeasons = ImmutableSet.<Season>builder().addAll(builder._historicSeasons).add(_currentSeason).build();
        _requiredHistory = builder._requiredHistory;
        _futureConsidered = builder._futureConsidered;
        _cacheTimeout = builder._cacheTimeout;
    }

    public static void main(String[] args) throws IOException, InstantiationException, IllegalAccessException, LpSolveException {
        Runner.builder(LinearRegressionModel.class, Season.S18_19)
                .addHistoricData(Season.S16_17)
                .addHistoricData(Season.S17_18)
//                .setCacheTimeout(Duration.ofDays(10))
                .build()
                .run();
    }

    private void run() throws IOException, IllegalAccessException, InstantiationException, LpSolveException {
        Map<Season, Set<Player>> seasonPlayers = new HashMap<>();
        Set<TrainingData> trainingData = new HashSet<>();
        GameWeek currentGW = null;

        for (Season season : _trainingSeasons) {
            DataSource dataSource = season.equals(_currentSeason) ?
                    Utils.loadPossiblyCachedDataSource(season.storedDataPath().toFile(), _cacheTimeout ) :
                    Utils.loadHistoricData(season.storedDataPath().toFile());

            try {

                Utils.registerTeams(dataSource.staticData());

                Map<String, Player> players = Utils.loadPlayers(dataSource.staticData());
                List<Fixture> fixtures = new ArrayList<>();

                for (GameWeek gameWeek : GameWeek.values()) {

                    Map<?, ?> gameWeekData = dataSource.gameWeekData().get(gameWeek);

                    if (gameWeekData == null) {
                        System.out.println("No data for gameweek " + gameWeek + ", breaking loop");
                        break;
                    }

                    Map<?, ?> gameWeekElements = (Map<?, ?>) gameWeekData.get("elements");
                    for (Map.Entry<String, Player> pEntry : players.entrySet()) {
                        Map<?, ?> playerGameWeekData = (Map<?, ?>) gameWeekElements.get(pEntry.getKey());
                        if (playerGameWeekData == null) {
                            continue;  // player wasn't in game this gameweek, or gameweek doesn't exist yet
                        }
                        pEntry.getValue().updateForGameWeek(gameWeek, (Map<?, ?>) playerGameWeekData.get("stats"));
                    }
                    List<Map<?, ?>> fixtureData = (List<Map<?, ?>>)gameWeekData.get("fixtures");
                    for (Map<?, ?> fixtureMap : fixtureData){
                        Team homeTeam = TeamsManager.fromId(Integer.toString(((Double)fixtureMap.get("team_h")).intValue()));
                        Team awayTeam = TeamsManager.fromId(Integer.toString(((Double)fixtureMap.get("team_a")).intValue()));
                        Integer homeScore = fixtureMap.get("team_h_score") == null ? null : ((Double)fixtureMap.get("team_h_score")).intValue();
                        Integer awayScore = fixtureMap.get("team_a_score") == null ? null : ((Double)fixtureMap.get("team_a_score")).intValue();
                        Fixture fixture = new Fixture(gameWeek, homeTeam, awayTeam, homeScore, awayScore);
                        fixtures.add(fixture);
                    }
                    List<Map<?, ?>> nextFixtureData = (List<Map<?, ?>>)dataSource.staticData().get("next_event_fixtures");
                    if (nextFixtureData != null) {
                        for (Map<?, ?> fixtureMap : nextFixtureData){
                            Team homeTeam = TeamsManager.fromId(Integer.toString(((Double)fixtureMap.get("team_h")).intValue()));
                            Team awayTeam = TeamsManager.fromId(Integer.toString(((Double)fixtureMap.get("team_a")).intValue()));
                            Integer homeScore = fixtureMap.get("team_h_score") == null ? null : ((Double)fixtureMap.get("team_h_score")).intValue();
                            Integer awayScore = fixtureMap.get("team_a_score") == null ? null : ((Double)fixtureMap.get("team_a_score")).intValue();
                            Fixture fixture = new Fixture(GameWeek.nextWeek(gameWeek), homeTeam, awayTeam, homeScore, awayScore);
                            fixtures.add(fixture);
                        }
                    }

                }

                // update teams with performance and fixture difficulty data

                for (GameWeek gameWeek : GameWeek.values()){

//                    if (gameWeek.toInt() < REQUIRED_HISTORY){
//                        continue; // todo: how to handle?
//                    }

                    Map<Team, Double> calculatedTeamStandard = Algorithms.calculateTeamRankings(
                            fixtures.stream()
                                    .filter(f -> f.gameWeek().toInt() <= gameWeek.toInt())  // only include 'seen' fixtures
                                    .collect(Collectors.toList()),
                            players.values().stream()
                                    .collect(Collectors.groupingBy(p -> p.team(gameWeek), Collectors.mapping(p -> p, Collectors.toList())))
                    );

                    for (Team team : TeamsManager.availableTeams()){

                        List<Fixture> previousFixtures = fixtures.stream()
                                .filter(f -> f.homeTeam().equals(team) || f.awayTeam().equals(team))
                                .filter(f -> f.gameWeek().toInt() <= gameWeek.toInt() && f.gameWeek().toInt() > gameWeek.toInt() - _requiredHistory)
                                .collect(Collectors.toList());

                        List<Fixture> futureFixtures = fixtures.stream()
                                .filter(f -> f.homeTeam().equals(team) || f.awayTeam().equals(team))
                                .filter(f -> f.gameWeek().toInt() > gameWeek.toInt() && f.gameWeek().toInt() <= gameWeek.toInt() + _futureConsidered)
                                .collect(Collectors.toList());

                        team.updateForGameWeek(gameWeek, players.values(), previousFixtures, futureFixtures, calculatedTeamStandard);
                    }
                }

                for (GameWeek gameWeek : GameWeek.values()) {

                    // todo: gw must be played

                    if (gameWeek.toInt() < _requiredHistory || gameWeek.equals(GameWeek.GW38) || (dataSource.staticData().get("next-event") != null && gameWeek.toInt() >= (Double)dataSource.staticData().get("next-event"))){
                        continue; // todo: how to handle?
                    }

                    for (Player p : players.values()) {

                        if (p.minutes(gameWeek) == null || p.minutes(gameWeek) < 30)
                            continue;  // not enough activity to train on, we reduce scope of operation to "playing players"

                        if (p.team(gameWeek).numberGames(GameWeek.nextWeek(gameWeek)) == 0)
                            continue;

                        FeatureVector featureVector = FeatureVectors.extractVector(gameWeek, p);
                        double label = p.points(GameWeek.nextWeek(gameWeek)).doubleValue();
                        trainingData.add(new TrainingData(featureVector, label, gameWeek, p));
                    }
                }

                seasonPlayers.put(season, ImmutableSet.copyOf(players.values()));

            } finally {
                if (season.equals(_currentSeason)) {
                    LocalDataSource.store(dataSource, _currentSeason.storedDataPath().toFile());
                    currentGW = GameWeek.fromInt(((Double)dataSource.staticData().get("next-event")).intValue());
                }
            }
        }

        // use the training data to build a model
        Model model = _modelClass.newInstance();
        model.train(trainingData);

        // todo: get a current feature vector for each player
        // only bother if player has match next week
        if (currentGW == null)
            throw new IllegalStateException();
        GameWeek gameweekNow = GameWeek.previousWeek(currentGW);  // go back one
        // todo: need to use "next event fixtures" in static data for this part
        // maybe update teams with this fixture as well?
        Map<Player, Double> predictions = seasonPlayers.get(_currentSeason).stream()
                .collect(Collectors.toMap(p -> p, p -> model.predict(p, FeatureVectors.extractVector(gameweekNow, p).getVector())));

        // todo remove this debug statement
//        for (Map.Entry<Player, Double> e : predictions.entrySet()) {
//            if (!e.getKey().position().equals(Position.DEF))
//                continue;
//            if (e.getKey().points(GameWeek.GW1) == 0)
//                continue;
//            System.out.println(e.getKey().surname() + "," + e.getKey().points(GameWeek.GW1) + "," + e.getValue());
//        }

        Collection<Player> team = Algorithms.optimizeSelection(predictions.keySet(), predictions::get, currentGW);
        for (Player p : team) {
            System.out.printf("%-30s: %-5s%n", p, predictions.get(p));
        }

    }

    private static Builder builder(Class<? extends Model> modelClass, Season currentSeason) {
        return new Builder(modelClass, currentSeason);
    }

    @SuppressWarnings("unused")
    private static class Builder {

        private final Class<? extends Model> _modelClass;
        private final Season _currentSeason;
        private final Set<Season> _historicSeasons = new HashSet<>();
        private int _requiredHistory = 10;
        private int _futureConsidered = 3;
        private Duration _cacheTimeout = Duration.ofMinutes(60);

        private Builder(Class<? extends Model> modelClass, Season currentSeason) {
            _modelClass = modelClass;
            _currentSeason = currentSeason;
        }

        /**
         * Add a season of historic data to be used for training.
         *
         * @param season the season to add
         * @return builder
         */
        public Builder addHistoricData(Season season) {
            _historicSeasons.add(season);
            return this;
        }

        /**
         * Sets the number of weeks in the season that must have been played before a datapoint is valid for use in the
         * training set. This will limit the size of the training set from the front-end of the season, in order to
         * allow time decayed features to stabilize.
         *
         * @param numWeeks the number of gameweeks of history required
         * @return builder
         */
        public Builder setRequiredHistory(int numWeeks) {
            _requiredHistory = numWeeks;
            return this;
        }

        /**
         * Sets the number of weeks in the future to consider when generating labels. A smaller value means less
         * reliability in the results, as single good weeks can represent a good datapoint for a mediocre player.
         * However, a higher value limits the size of the training data from the top end.
         *
         * @param numWeeks the number of gameweeks of future to consider
         * @return builder
         */
        public Builder setFutureConsidered(int numWeeks) {
            _futureConsidered = numWeeks;
            return this;
        }

        /**
         * Set the age that the current season data file can be before it is renewed with further API calls. Set this
         * to avoid spamming the FPL server with requests and to improve data load time.
         *
         * @param timeout cache timeout
         * @return builder
         */
        public Builder setCacheTimeout(Duration timeout) {
            _cacheTimeout = timeout;
            return this;
        }

        public Runner build() {
            return new Runner(this);
        }

    }

}
