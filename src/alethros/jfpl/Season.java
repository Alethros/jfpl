package alethros.jfpl;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Supported seasons and associated information
 */
public enum Season {

    S14_15(Paths.get("resources", "FPL_2014-15.json")),
    S16_17(Paths.get("resources", "FPL_2016-17.json")),
    S17_18(Paths.get("resources", "FPL_2017-18.json")),
    S18_19(Paths.get("resources", "FPL_2018-19.json")),
    S19_20(Paths.get("resources", "FPL_2019-20.json"));

    private final Path _storedDataPath;

    Season(Path storedDataPath) {
        _storedDataPath = storedDataPath;
    }

    public Path storedDataPath() {
        return _storedDataPath;
    }
}
