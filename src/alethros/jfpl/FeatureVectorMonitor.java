package alethros.jfpl;

import alethros.jfpl.visualization.Charts;

import java.util.*;
import java.util.stream.Collectors;

public class FeatureVectorMonitor {

    private static final Map<Position, List<double[]>> positionalFeatureVectorsTrain = new HashMap<>();
    private static final Map<Position, List<double[]>> positionalFeatureVectorsTest = new HashMap<>();

    public static void registerTrainingSet(Position p, List<TrainingData> trainingData){
        List<double[]> featureVectors = trainingData.stream().map(t -> t.featureVector().getVector()).collect(Collectors.toList());
        positionalFeatureVectorsTrain.put(p, featureVectors);
    }

    public static void registerTestSet(Position p, List<double[]> featureVectors){
        positionalFeatureVectorsTest.put(p, featureVectors);
    }

    public static void analyse(){
        for (Position p : Position.values()){
            int exampleLength = positionalFeatureVectorsTrain.get(p).get(0).length;
            for (int i = 0; i < exampleLength; i++){
                int finalI = i;
                List<Double> featureDistTrain = positionalFeatureVectorsTrain.get(p).stream().map(darr -> darr[finalI]).collect(Collectors.toList());
                List<Double> featureDistTest = positionalFeatureVectorsTest.get(p).stream().map(darr -> darr[finalI]).collect(Collectors.toList());
                plotHistoComparison(featureDistTrain, featureDistTest);
            }
//            if (p.shortName.equals("GKP")) {
//                System.out.println("Position: " + p.shortName);
//                System.out.println("TRAIN");
//                for (double[] d : positionalFeatureVectorsTrain.get(p)) {
//                    StringBuilder sb = new StringBuilder();
//                    for (int i = 0; i < d.length; i++) {
//                        sb.append(d[i]).append(",");
//                    }
//                    System.out.println(sb.toString());
//                }
//                System.out.println("TEST");
//                for (double[] d : positionalFeatureVectorsTest.get(p)) {
//                    StringBuilder sb = new StringBuilder();
//                    for (int i = 0; i < d.length; i++) {
//                        sb.append(d[i]).append(",");
//                    }
//                    System.out.println(sb.toString());
//                }
//            }
        }
    }

    private static void plotHistoComparison(List<Double> a, List<Double> b){

        final int nBins = 40;

        List<Double> c = new ArrayList<>(a);
        c.addAll(b);
        c.sort(Comparator.comparingDouble(d -> d));
        double min = c.get(0);
        double max = c.get(c.size() - 1);
        double[] bins = new double[nBins];
        for (int i = 0; i < nBins; i++){
            bins[i] = (i+1) * (max - min) / nBins + min;
        }
        double[] aBinned = new double[nBins];
        Arrays.fill(aBinned, 0);
        for (Double d : a){
            for (int i = 0; i < nBins; i++){
                if (d < bins[i]){
                    aBinned[i] += 1;
                    break;
                }
            }
        }
        double[] bBinned = new double[nBins];
        Arrays.fill(bBinned, 0);
        for (Double d : b){
            for (int i = 0; i < nBins; i++){
                if (d < bins[i]){
                    bBinned[i] += 1;
                    break;
                }
            }
        }

        double normalization = ((double)a.size()) / b.size();
        for (int i = 0; i < nBins; i++){
            bBinned[i] *= normalization;
        }

        double[] aArr = new double[a.size()];
        for (int i = 0; i < a.size(); i++){
            aArr[i] = a.get(i);
        }
        double[] bArr = new double[b.size()];
        for (int i = 0; i < b.size(); i++){
            bArr[i] = b.get(i);
        }

//        new ScatterPlot("featureDistTrain", bins, aBinned);
        Charts.createScatter("featureDist", bins, aBinned, bBinned);
        Charts.createScatter("a vs b", aArr, bArr);
        Charts.awaitAcknowledgement();
    }

}
