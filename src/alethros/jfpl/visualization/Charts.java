package alethros.jfpl.visualization;

import com.google.common.primitives.Doubles;

import java.util.*;

public abstract class Charts {

    private Charts(){}

    public static void createScatter(String name, double[] x, double[] y){
        new ScatterPlot(name, x, y);
    }

    public static void createScatter(String name, double[] x, double[] y1, double[] y2){
        new ScatterPlot(name, x, y1, y2);
    }

    public static void createHistogram(String name, int nBins, double[] vals){
        double[] bins = determineBinArray(nBins, vals);
        double[] binned = new double[nBins];
        Arrays.fill(binned, 0);
        for (Double d : vals){
            for (int i = 0; i < nBins; i++){
                if (d < bins[i]){
                    binned[i] += 1;
                    break;
                }
            }
        }
        createScatter(name, bins, binned);
    }

    public static void awaitAcknowledgement(){
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
    }

    private static double[] determineBinArray(int nBins, double[] vals){
        List<Double> c = Doubles.asList(vals);
        c.sort(Comparator.comparingDouble(d -> d));
        double min = c.get(0);
        double max = c.get(c.size() - 1);
        double[] bins = new double[nBins];
        for (int i = 0; i < nBins; i++){
            bins[i] = (i+1) * (max - min) / nBins + min;
        }
        return bins;
    }

}
