package alethros.jfpl.visualization;

import org.math.plot.Plot2DPanel;

import javax.swing.*;

public class ScatterPlot{


    ScatterPlot(String name, double[] x, double[] y) {
        // create your PlotPanel (you can use it as a JPanel)
        Plot2DPanel plot = new Plot2DPanel();

        // add a line plot to the PlotPanel
        plot.addScatterPlot(name, x, y);

        // put the PlotPanel in a JFrame, as a JPanel
        JFrame frame = new JFrame(name);
        frame.setContentPane(plot);
        frame.setBounds(100, 100, 1000, 1000);
        frame.setVisible(true);
    }

    ScatterPlot(String name, double[] x, double[] y1, double[] y2){
        // create your PlotPanel (you can use it as a JPanel)
        Plot2DPanel plot = new Plot2DPanel();

        // add a line plot to the PlotPanel
        plot.addBarPlot(name, x, y1);
        plot.addBarPlot(name, x, y2);

        // put the PlotPanel in a JFrame, as a JPanel
        JFrame frame = new JFrame(name);
        frame.setContentPane(plot);
        frame.setBounds(100, 100, 1000, 1000);
        frame.setVisible(true);
    }
}