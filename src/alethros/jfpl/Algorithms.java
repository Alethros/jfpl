package alethros.jfpl;

import alethros.jfpl.entities.Player;
import alethros.jfpl.entities.Team;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.joptimizer.exception.JOptimizerException;
import com.joptimizer.functions.ConvexMultivariateRealFunction;
import com.joptimizer.functions.LinearMultivariateRealFunction;
import com.joptimizer.functions.PDQuadraticMultivariateRealFunction;
import com.joptimizer.optimizers.JOptimizer;
import com.joptimizer.optimizers.OptimizationRequest;
import lpsolve.LpSolve;
import lpsolve.LpSolveException;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Algorithms {

    private enum Formation {
        F343(3, 4, 3), F352(3, 5, 2), F433(4, 3, 3), F442(4, 4, 2), F451(4, 5, 1), F523(5, 2, 3), F532(5, 3, 2), F541(5, 4, 1);

        final int _def;
        final int _mid;
        final int _fwd;

        Formation(int def, int mid, int fwd){
            _def = def;
            _mid = mid;
            _fwd = fwd;
        }

        public int getNumberGoalkeepers(){
            return 1;
        }

        public int getNumberDefenders(){
            return _def;
        }

        public int getNumberMidfielders(){
            return _mid;
        }

        public int getNumberForwards(){
            return _fwd;
        }
    }

    public static <T extends Collection<Player>> Collection<Player> optimizeSelection(T playersColl, Function<Player, Double> objectiveFunction, GameWeek gw) throws LpSolveException {

        List<Player> players = new ArrayList<>(playersColl);  // definite ordering
        List<String> banned = Lists.newArrayList("Trippier", "Hemed", "SanÃ©", "Sterling", "Niasse", "Lacazette");
        players = players.stream().filter(p -> !banned.contains(p.surname())).collect(Collectors.toList());

        final double[] objectives = new double[players.size()];
        final double[] prices = new double[players.size()];
        final double[] isGoalkeeper = new double[players.size()];
        final double[] isDefender = new double[players.size()];
        final double[] isMidfielder = new double[players.size()];
        final double[] isForward = new double[players.size()];
        final Map<Team, double[]> isTeams = new LinkedHashMap<>();
        for (Team t : TeamsManager.availableTeams()){
            isTeams.put(t, new double[players.size()]);
        }

        for (int i =0; i < players.size(); i++){
            Player player = players.get(i);
            objectives[i] = objectiveFunction.apply(player);
            isGoalkeeper[i] = player.position().equals(Position.GKP) ? 1 : 0;
            isDefender[i] = player.position().equals(Position.DEF) ? 1 : 0;
            isMidfielder[i] = player.position().equals(Position.MID) ? 1 : 0;
            isForward[i] = player.position().equals(Position.FWD) ? 1 : 0;
            prices[i] = player.cost();
            Team t = player.team(gw);
            for (Team tt : isTeams.keySet()){
                isTeams.get(tt)[i] = t.equals(tt) ? 1. : 0.;
            }
        }

        Map<Formation, LpSolve> solvers = new HashMap<>();
        Map<Formation, List<Player>> benches = new HashMap<>();

        for (Formation f : Formation.values()) {
            LpSolve solve = LpSolve.makeLp(0, players.size());

            // positions
            solve.strAddConstraint(arrayToString(isGoalkeeper), LpSolve.EQ, f.getNumberGoalkeepers());
            solve.strAddConstraint(arrayToString(isDefender), LpSolve.EQ, f.getNumberDefenders());
            solve.strAddConstraint(arrayToString(isMidfielder), LpSolve.EQ, f.getNumberMidfielders());
            solve.strAddConstraint(arrayToString(isForward), LpSolve.EQ, f.getNumberForwards());

            // total cost
            List<Player> bench = new ArrayList<>();
            addToBench(bench, players, Position.GKP, Position.GKP.expectedNumber() - f.getNumberGoalkeepers());
            addToBench(bench, players, Position.DEF, Position.DEF.expectedNumber() - f.getNumberDefenders());
            addToBench(bench, players, Position.MID, Position.MID.expectedNumber() - f.getNumberMidfielders());
            addToBench(bench, players, Position.FWD, Position.FWD.expectedNumber() - f.getNumberForwards());

            if (bench.size() != 4){
                throw new IllegalStateException("Unexpected number on bench: " + bench.size());
            }

            benches.put(f, ImmutableList.copyOf(bench));

            double benchCost = bench.stream().map(Player::cost).mapToDouble(d -> (double)d).sum();

            solve.strAddConstraint(arrayToString(prices), LpSolve.LE, 1000 - benchCost);
            solve.setDebug(false);
            solve.setOutputfile("lp.log");  // else tons of debug output to screen...

            // teams
            for (Map.Entry<Team, double[]> entry : isTeams.entrySet()) {
                solve.strAddConstraint(arrayToString(entry.getValue()), LpSolve.LE, 3);
            }

            // objective
            solve.strSetObjFn(arrayToString(objectives));
            solve.setMaxim();

            // integer outcome
            for (int i = 0; i < players.size(); i++) {
                solve.setInt(i + 1, true);  // n.b. col not zero-based
                solve.setBounds(i + 1, 0, 1);
            }
            solvers.put(f, solve);
        }

        final Map<Formation, LpSolve> _solvers = ImmutableMap.copyOf(solvers);
        final Map<Formation, List<Player>> _bench = ImmutableMap.copyOf(benches);

        double maxObjectiveSoFar = 0;
        Map.Entry<Formation, LpSolve> maxCameFrom = null;
        for (Map.Entry<Formation, LpSolve> entry : _solvers.entrySet()) {
            int ret = entry.getValue().solve();
            if (ret != LpSolve.OPTIMAL) {
                throw new IllegalStateException("LpSolve not optimal: " + ret);
            }

            // print solution
            double objectiveValue = entry.getValue().getObjective();
            System.out.printf("Value of objective function for formation %s: %s\n", entry.getKey(), objectiveValue);
            if (objectiveValue > maxObjectiveSoFar){
                maxObjectiveSoFar = objectiveValue;
                maxCameFrom = entry;
            }
        }

        if (maxCameFrom == null){
            throw new IllegalStateException("No solution with positive outcome found");
        }

        System.out.println("Max entry is for formation " + maxCameFrom.getKey());

        List<Player> team = new ArrayList<>();
        double[] var = maxCameFrom.getValue().getPtrVariables();
        for (int i = 0; i < var.length; i++) {
            if (var[i] != 0.0) {
//                System.out.println("Value of var[" + i + "] = " + var[i] + " -> " + players.get(i));
                team.add(players.get(i));
            }
        }

        // delete the problem and free memory
        for (Map.Entry<Formation, LpSolve> entry : _solvers.entrySet()) {
            entry.getValue().deleteLp();
        }

        team.addAll(_bench.get(maxCameFrom.getKey()));
        return ImmutableList.copyOf(team);
    }

    public static <T extends Collection<Player>> void optimizeSeasonSelection(T playersColl) throws JOptimizerException {

        // Objective function
        double[][] P = new double[][] {{ 5., 1., 4. }, { 3., 7., 5. }, { 2., 5., 4.}};
        PDQuadraticMultivariateRealFunction objectiveFunction = new PDQuadraticMultivariateRealFunction(P, null, 0);

        //equalities
        double[][] A = new double[][]{{1, 1, 1}};
        double[] b = new double[]{2};

        //inequalities
        ConvexMultivariateRealFunction[] inequalities = new ConvexMultivariateRealFunction[2];
        inequalities[0] = new LinearMultivariateRealFunction(new double[]{-1, 0}, 0);
        inequalities[1] = new LinearMultivariateRealFunction(new double[]{0, -1}, 0);

        //optimization problem
        OptimizationRequest or = new OptimizationRequest();
        or.setF0(objectiveFunction);
        or.setInitialPoint(new double[] { 0.1, 0.9, 1.0});
        //or.setFi(inequalities); //if you want x>0 and y>0
        or.setA(A);
        or.setB(b);
        or.setToleranceFeas(1.E-12);
        or.setTolerance(1.E-12);

        //optimization
        JOptimizer opt = new JOptimizer();
        opt.setOptimizationRequest(or);
        opt.optimize();

        double[] sol = opt.getOptimizationResponse().getSolution();
        System.out.println(Arrays.toString(sol));

    }

    public static Map<Team, Double> calculateTeamRankings(Collection<Fixture> fixtures, Map<Team, List<Player>> teamPlayers){
        // todo: clever algorithm, for now return base pts for each
        return fixtures.stream()
                .map(f -> Arrays.asList(f.homeTeam(), f.awayTeam()))
                .flatMap(Collection::stream)
                .distinct()
                .collect(Collectors.toMap(t -> t, t -> fixtures.stream()
                        .filter(f -> f.homeTeam().equals(t) || f.awayTeam().equals(t))
                        .map(f -> {
                            boolean isHome = f.homeTeam().equals(t);
                            Team opponent = isHome ? f.awayTeam() : f.homeTeam();
                            return teamPlayers.get(opponent).stream()
                                    .map(p -> p.points(f.gameWeek()))
                                    .filter(Objects::nonNull)
                                    .mapToInt(i -> i)
                                    .sum();
                        })
                        .mapToDouble(v -> v)
                        .average().orElseThrow(IllegalStateException::new)
                ));
    }

    private static String arrayToString(double[] arr){
        if (arr.length == 0)
            return "";
        StringBuilder sb = new StringBuilder(Double.toString(arr[0]));
        for (int i = 1; i < arr.length; i++){
            sb.append(" ").append(arr[i]);
        }
        return sb.toString();
    }

    private static void addToBench(List<Player> bench, List<Player> playerPool, Position position, int nToAdd){
        List<Player> eligible = new ArrayList<>();
        for (Player p : playerPool){
            if (p.position().equals(position)){
                eligible.add(p);
            }
        }
        eligible.sort((a, b) -> a.cost() > b.cost() ? 1 : b.cost() > a.cost() ? -1 : 0);
        for (int i = 0; i < nToAdd; i++){
            bench.add(eligible.get(i));
        }
    }

}
