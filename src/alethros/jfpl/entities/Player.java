package alethros.jfpl.entities;

import alethros.jfpl.GameWeek;
import alethros.jfpl.Position;
import alethros.jfpl.TeamsManager;

import java.util.HashMap;
import java.util.Map;

public class Player implements Entity {

    private final String _id;
    private final String _name;
    private final String _fullName;
    private final int _cost;  // n.b this is static at the time of data extraction, and not reflective of a weekly value
    private final int _startCost;
    private final Team _team;  // todo: this can change early on, or in January transfer - how to handle?
    private final Position _position;  // we assert that this is a constant within an FPL season

    private final Map<GameWeek, Integer> _points;
    private final Map<GameWeek, Integer> _minutes;

    private final Map<GameWeek, Integer> _goalsScored;
    private final Map<GameWeek, Integer> _assists;
    private final Map<GameWeek, Integer> _cleanSheets;  // def/gkp only
    private final Map<GameWeek, Integer> _saves;        // gkp only
    private final Map<GameWeek, Integer> _penaltySaves; // gkp only

    private final Map<GameWeek, Integer> _goalsConceded;
    private final Map<GameWeek, Integer> _yellows;
    private final Map<GameWeek, Integer> _reds;
    private final Map<GameWeek, Integer> _penaltyMisses;
    private final Map<GameWeek, Integer> _ownGoals;

    private final Map<GameWeek, Integer> _bps;
    private final Map<GameWeek, Double> _influence;
    private final Map<GameWeek, Double> _creativity;
    private final Map<GameWeek, Double> _threat;

    public Player(Map<String, ?> staticData){

        _id = Integer.toString(((Double) staticData.computeIfAbsent("id", k -> {
            throw new IllegalArgumentException("Missing data in " + staticData);
        })).intValue());
        _name = (String) staticData.computeIfAbsent("second_name", k -> {
            throw new IllegalArgumentException("Missing data in " + staticData);
        });
        _fullName = staticData.computeIfAbsent("first_name", k -> {
            throw new IllegalArgumentException("Missing data in " + staticData);
        }) + " " + _name;
        _cost = ((Double) staticData.computeIfAbsent("now_cost", k -> {
            throw new IllegalArgumentException("Missing data in " + staticData);
        })).intValue();
        _startCost = _cost - (((Double) staticData.computeIfAbsent("cost_change_start", k -> {
            throw new IllegalArgumentException("Missing data in " + staticData);
        })).intValue());
        _team = TeamsManager.fromId(Integer.toString(((Double) staticData.computeIfAbsent("team", k -> {
            throw new IllegalArgumentException("Missing data in " + staticData);
        })).intValue()));
        _position = Position.fromId(((Double) staticData.computeIfAbsent("element_type", k -> {
            throw new IllegalArgumentException("Missing data in " + staticData);
        })).intValue());

        _points = new HashMap<>();
        _minutes = new HashMap<>();

        _goalsScored = new HashMap<>();
        _assists = new HashMap<>();
        _cleanSheets = new HashMap<>();
        _saves = new HashMap<>();
        _penaltySaves = new HashMap<>();

        _goalsConceded = new HashMap<>();
        _yellows = new HashMap<>();
        _reds = new HashMap<>();
        _penaltyMisses = new HashMap<>();
        _ownGoals = new HashMap<>();

        _bps = new HashMap<>();
        _influence = new HashMap<>();
        _creativity = new HashMap<>();
        _threat = new HashMap<>();
    }

    public void updateForGameWeek(GameWeek gw, Map<?, ?> gwStats) {

        // get relevant values
        _points.put(gw, ((Double) gwStats.get("total_points")).intValue());
        _minutes.put(gw, ((Double) gwStats.get("minutes")).intValue());
        _goalsScored.put(gw, ((Double) gwStats.get("goals_scored")).intValue());
        _assists.put(gw, ((Double) gwStats.get("assists")).intValue());
        _cleanSheets.put(gw, ((Double) gwStats.get("clean_sheets")).intValue());
        _saves.put(gw, ((Double) gwStats.get("saves")).intValue());
        _penaltySaves.put(gw, ((Double) gwStats.get("penalties_saved")).intValue());
        _goalsConceded.put(gw, ((Double) gwStats.get("goals_conceded")).intValue());
        _yellows.put(gw, ((Double) gwStats.get("yellow_cards")).intValue());
        _reds.put(gw, ((Double) gwStats.get("red_cards")).intValue());
        _penaltyMisses.put(gw, ((Double) gwStats.get("penalties_missed")).intValue());
        _ownGoals.put(gw, ((Double) gwStats.get("own_goals")).intValue());
        _bps.put(gw, ((Double) gwStats.get("bps")).intValue());
        _influence.put(gw, (Double) gwStats.get("influence"));
        _creativity.put(gw, (Double) gwStats.get("creativity"));
        _threat.put(gw, (Double) gwStats.get("threat"));

        // todo: what to do if some previous gameweeks missed?

        // todo: update based on these
    }

    public String surname() {
        return _name;
    }

    public String fullName() {
        return _fullName;
    }

    public int cost() {
        return _cost;
    }

    public int startCost() {
        return _startCost;
    }

    public Team team(GameWeek gw) {
        return _team;
    }

    public Position position() {
        return _position;
    }

    public Integer points(GameWeek gw) {
        return _points.get(gw);
    }

    public Integer minutes(GameWeek gw) {
        return _minutes.get(gw);
    }

    public Integer goalsScored(GameWeek gw) {
        return _goalsScored.get(gw);
    }

    public Integer assists(GameWeek gw) {
        return _assists.get(gw);
    }

    public Integer cleanSheets(GameWeek gw) {
        return _cleanSheets.get(gw);
    }

    public Integer saves(GameWeek gw) {
        return _saves.get(gw);
    }

    public Integer penaltySaves(GameWeek gw) {
        return _penaltySaves.get(gw);
    }

    public Integer goalsConceded(GameWeek gw) {
        return _goalsConceded.get(gw);
    }

    public Integer yellows(GameWeek gw) {
        return _yellows.get(gw);
    }

    public Integer reds(GameWeek gw) {
        return _reds.get(gw);
    }

    public Integer penaltyMisses(GameWeek gw) {
        return _penaltyMisses.get(gw);
    }

    public Integer ownGoals(GameWeek gw) {
        return _ownGoals.get(gw);
    }

    public Integer bps(GameWeek gw) {
        return _bps.get(gw);
    }

    public Double influence(GameWeek gw) {
        return _influence.get(gw);
    }

    public Double creativity(GameWeek gw) {
        return _creativity.get(gw);
    }

    public Double threat(GameWeek gw) {
        return _threat.get(gw);
    }

    @Override
    public String toString(){
        return _name;
    }

}
