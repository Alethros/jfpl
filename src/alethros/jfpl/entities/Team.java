package alethros.jfpl.entities;

import alethros.jfpl.Fixture;
import alethros.jfpl.GameWeek;
import javafx.util.Pair;

import java.util.*;
import java.util.stream.Collectors;

public class Team implements Entity {

    private final String _id;
    private final String _name;
    private final String _shortName;

    private final Map<GameWeek, Integer> _numberGames;
    private final Map<GameWeek, List<Pair<Fixture, Double>>> _previousFixtureDifficulties;
    private final Map<GameWeek, List<Pair<Fixture, Double>>> _futureFixtureDifficulties;

    public Team(String id, String name, String shortName, int strength){
        _id = id;
        _name = name;
        _shortName = shortName;

        _numberGames = new HashMap<>();
        _previousFixtureDifficulties = new HashMap<>();
        _futureFixtureDifficulties = new HashMap<>();
    }

    public void updateForGameWeek(GameWeek gw, Collection<Player> players, List<Fixture> previousFixtures, List<Fixture> futureFixtures, Map<Team, Double> teamDifficulties){

        Set<Player> teamPlayers = players.stream().filter(p -> p.team(gw).equals(this)).collect(Collectors.toSet());
        Set<Fixture> fixtures = previousFixtures.stream().filter(f -> f.gameWeek().equals(gw)).collect(Collectors.toSet());

        _numberGames.put(gw, teamPlayers.stream().map(p -> p.minutes(gw)).filter(Objects::nonNull).mapToInt(i -> i).max().orElse(0) / 90);
        _previousFixtureDifficulties.put(gw, previousFixtures.stream().map(f -> new Pair<>(f, teamDifficulties.get(f.homeTeam().equals(this) ? f.awayTeam() : f.homeTeam()))).collect(Collectors.toList()));
        _futureFixtureDifficulties.put(gw, futureFixtures.stream().map(f -> new Pair<>(f, teamDifficulties.get(f.homeTeam().equals(this) ? f.awayTeam() : f.homeTeam()))).collect(Collectors.toList()));

        // todo team form?

    }

    public String id() {
        return _id;
    }

    public String shortName() {
        return _shortName;
    }

    public Integer numberGames(GameWeek gw) {
        return _numberGames.get(gw);
    }

    public Set<Double> previousFixtureDifficulties(GameWeek currentWeek, GameWeek targetWeek) {
        return _previousFixtureDifficulties.get(currentWeek).stream().filter(p -> p.getKey().gameWeek().equals(targetWeek)).map(Pair::getValue).collect(Collectors.toSet());
    }

    public Set<Double> futureFixtureDifficulties(GameWeek currentWeek, GameWeek targetWeek) {
        return _futureFixtureDifficulties.get(currentWeek).stream().filter(p -> p.getKey().gameWeek().equals(targetWeek)).map(Pair::getValue).collect(Collectors.toSet());
    }

    @Override
    public String toString() {
        return _name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Team team = (Team) o;

        if (!_id.equals(team._id)) return false;
        if (!_name.equals(team._name)) return false;
        return _shortName.equals(team._shortName);
    }

    @Override
    public int hashCode() {
        int result = _id.hashCode();
        result = 31 * result + _name.hashCode();
        result = 31 * result + _shortName.hashCode();
        return result;
    }
}
